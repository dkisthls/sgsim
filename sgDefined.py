import numpy
from enum import Enum, auto  # Used for status indicators, sequence actions

###------------------------------ sgCommands -----------------------------------
class sgCommands:
  # Primary connection commands
  INIT     = 'init'        # SG
  BEGIN    = 'begin'       # SG
  END      = 'end'         # SG
  READOUT  = 'readout'     # SG
  STANDBY  = 'standby'     # SG
  STATE    = 'state'       # SG
  DATA     = 'data'        # Sim Only
  SHUTDOWN = 'shutdown'    # Sim Only

  # Secondary connection commands
  # NOTE: The socket recv() for the secondary connection will receive 
  #       a fixed number of bytes as defined by const.SECONDARY_BUF_SIZE.
  #       All commands on the secondary connection must be the same length
  #       hence the filler '_' characters on 'simtrig__' and 'rsimtrig_'.
  INTERRUPT = 'interrupt'  # SG
  SIMTRIG   = 'simtrig__'  # Sim HW trigger
  RSIMTRIG  = 'rsimtrig_'  # Sim Reset file index and sim HW trigger

class sgResponse:
  # Indexes the different prefixes that are valid as the the first character of
  # a response from the server.
  class PREFIX(Enum):
      CONTINUE = 1
      ERROR = 2
      GET = 3
      OK = 4

  # Strings for response prefixes including required whitespace 
  PREFIX_STR = { PREFIX.CONTINUE.value : '+ ',
                 PREFIX.ERROR.value: '! ',
                 PREFIX.GET.value: '# ',
                 PREFIX.OK.value: '. ' }

###------------------------------- sgStates ------------------------------------
class sgStates:
  # The StarGrasp interface states
  STANDBY = 'standby'  # Camera detector is in standby, system is not operable. 'init' required.
  READY   = 'ready'    # Camera has been initialised and is ready for use.
  ACTIVE  = 'active'   # Camera is actively reading out.

###------------------------------ sgConstants ----------------------------------
class sgConstants:
  DEBUG_SG_CMD_READOUT = False
  DEBUG_PRINT_CMDLINE_ARGS = False
  DEBUG_LOG_SEQUENCE_ENTRIES = False
  DEBUG_FIGURE_TIMING = False
  DEBUG_EXECUTE_SEQUENCE = False
  DEBUG_FITSPIPE_THREAD = False
  DEBUG_SOCKET = False
  
  BIT_TYPE = numpy.uint16       # Allowed bit depths and the numpy equivalents
  SENSOR_WIDTH  = 2048          # Sensor array width(x) in pixels
  SENSOR_HEIGHT = 2048          # Sensor array height(y) in pixels
  MAX_FILES     = 100000        # Max num files before rolling back to 0
  MAX_SEQUENCES = 100000        # Max num sequences before rolling back to 0
  MAX_SEQ_LEN   = 750           # Max chars in a sequence string
  MAX_SEQ_ENTRIES = 50          # Max comma delimited entries in a sequence
  START_OF_COMMENT = '#'

  ONE_THOUSAND = 1000
  MILLISECONDS_PER_SECOND = ONE_THOUSAND
  MICROSECONDS_PER_SECOND = MILLISECONDS_PER_SECOND * ONE_THOUSAND
  NANOSECONDS_PER_SECOND = MICROSECONDS_PER_SECOND * ONE_THOUSAND
  ONE_MILLISECOND = 1.0/MILLISECONDS_PER_SECOND  # one millisecond (in seconds)
  ONE_MICROSECOND = 1.0/MICROSECONDS_PER_SECOND  # one microsecond (in seconds)

  # Minimum delay supported by Stargrasp (microseconds)
  MIN_DELAY_US = 1

  # Define the run modes for the camera
  UP_THE_RAMP = 0
  SUB_FRAME   = 1
  ALLOWED_MODES = ['UpTheRamp', 'SubFrame']

  # The minimum number of resets/reads for 'UpTheRamp' or subframe reads for 'SubFrame'.
  MIN_NDRS = {'UpTheRamp': 2, 'SubFrame': 1}

  # Frame generator modes
  ALLOWED_FRAMEGEN_MODES = ["Off", "ColumnCount", "FixedPixel", "FrameCount", "RowCount"]

  # The allowed "processing time" for a readout command
  T_READOUT_CMD = 0.002

  PRIMARY_BUF_SIZE = 4096    # Primary command socket buffer size
  SECONDARY_BUF_SIZE = 10    # Secondary interrupt/simtrig buffer size

  PRIME_FITSPIPE_WAIT = 1.0  # Time to wait after writing first file to fitspipe
                             # during camera startup to prime fitspipe

  THREAD_START_TIMEOUT = 1.0 # Max time to wait for a thread to start(sec)


###------------------------------ sgDefaults -----------------------------------
class sgDefaults:
  # Default sgSim server host and port.
  SERVER_HOST = 'localhost'
  SERVER_PORT = 8000

  # Default host and port that sgSim will use to connect to a fitspipe.
  FITSPIPE_HOST = 'localhost'
  FITSPIPE_PORT = 9999

  # Default path to ramdisk tmpfs used for temporary storage of fits files
  # created by sgSim and its integrated frame-generator."  
  # Configurable from command line with: --tmpfspath
  PATH_TO_TMPFS = '/tmp/dl_tmpfs'

  # Default path and file name for simulator logging
  LOG_FILE = '/tmp/dl_sgserver.log'

  # Default directory where the fitspipe app is located.
  # sgSim will start it's own copy of fitspipe running and then connect to it
  # Configurable from command line with: --fpbinpath
  PATH_TO_FITSPIPE = '$ATST/tools/x86_64/stargrasp/bin/'

  # fitspipe binary name
  FITSPIPE_BIN_NAME = 'fitspipe'

  # Value for fitspipe 'daemon=' command line argument.
  # Controls whether fitspipe will be started as a daemon process.
  FITSPIPE_DAEMON = 'false'      # Valid values: 'true' or 'false'

  # FITSPIPE debug
  # If setting DEBUG_FITSPIPE = True be sure to verify fitspipe supports the
  # 'verbose' flag.
  DEBUG_FITSPIPE = False         # Enables debug (inclusion of 'verbose' flag)

  # Value for fitspipe 'verbose=' command line argument.
  # Only works when 'daemon=false' and 'daemon' is supported by the version of
  # fitspipe being used.
  FITSPIPE_DEBUG_VERBOSITY = 0   # Valid values: 0 (off) through 4

  # Camera mode
  MODE = sgConstants.ALLOWED_MODES[sgConstants.UP_THE_RAMP]

  FIXED_PIXEL_VALUE = 1024                               # Default fixed pixel value
  FRAMEGEN_MODE = sgConstants.ALLOWED_FRAMEGEN_MODES[3]  # Default framegen mode

  # Maximum delay supported by Stargrasp (microseconds)
  MAX_DELAY_US = 500000

  # Amount of time, in seconds, for an init/standby command to complete 
  # Note: This time value is for simulation purposes only and may not reflect 
  #       the actual initialization or standby time of the camera hardware.
  INIT_TIME = 15.0

  # Amount of time, in seconds, to wait between sending "in progress" message updates
  IN_PROGRESS_UPDATE_INTERVAL = 10.0

  # Amount of time (in seconds) to wait for a fitspipe to become available
  FITSPIPE_CONNECT_TIMEOUT = 5

  MIN_SUBFRAME_PERIOD = 3        # Minimum subframe period (i.e. number of rows)
  MAX_SUBFRAME_PERIOD = 2047     # Maximum subframe period (i.e. number of rows)
  T_FREAD = 34013605             # Time to perform a frame read of the array(in ns).
  T_FRST = 0                     # Time to reset all FPGAs to state 0(in ns).
  T_GRST = 34013605              # Time to perform a global reset of the array(in ns).
  T_LRST = 34013605              # Time to perform a line reset of the array(in ns).
  T_TEND = 0                     # Time at end of ramp (i.e. delivery latency, other)
  T_TLAT = 620                   # Default trigger latency times (in ns)

  # Subframe integration defines
  T_SEOVH = 0                    # Subframe exposure time calculation constant (contingency) (in ns)
  T_SREAD = T_FREAD * 2          # Time to perform a subframe integration read of the array(in ns).
  T_SRINT = T_SREAD / (2049 * 2) # Subframe row integration time(in ns)

  # Default wait time (seconds) to simulate the time it takes SGServer to return
  # from a 'readout' command
  T_READOUT = sgConstants.T_READOUT_CMD

  # Default wait time (seconds) to simulate the delay between the "OK" and 
  # "INTERRUPTED" responses for an "interrupt" command.
  T_INTERRUPT = 0.100

  