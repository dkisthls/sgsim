# Tests all valid readout sequence actions in both long form
# ('line', 'global', 'read', 'subframe', 'msec', 'usec') and short form 
# ('l', 'g', 'r', 's', 'm', 'u').
# Tests are performed on all permutations of case and all combinations
# of actions with/without associated counts. 
# 
# Dependency: Requires sgReadoutCmdUtil.py, the readout command helper
#             for sgSim.py.
# To run: $ python3 test_sgReadoutCmdUtil.py
# Duration: Test takes ~21-22 seconds

### Python Library imports ###
import itertools
import random
import unittest

### Stargrasp Sim Imports ###
from sgReadoutCmdUtil import ReadoutHelper
from sgDefined import sgConstants as const
from sgDefined import sgDefaults as default

simpleActions = ['trig']                     # can be single char or word
delays        = ['msec', 'usec']             # can be single char or word, optional count
readActions   = ['line', 'global', 'read', 'subframe']   # can be single char or word
trigActions   = ['trigline', 'trigglobal', 'trigread', 'trigsubframe']
invalidCounts = [-1, 0]
maxDelayCount = {'m': default.MAX_DELAY_US / const.MILLISECONDS_PER_SECOND,\
                 'u': default.MAX_DELAY_US}

# Create lists of short/long forms of delays and read actions
allValidDelays = delays + [delay[0] for delay in delays]
allValidReadActions = readActions + [action[0] for action in readActions]


class SequenceTestCase(unittest.TestCase):
  """Tests readout command sequence strings."""
  def permutateCase(self, word):
    # The followoing algorithm using itertools and zip was obtained from 
    # https://stackoverflow.com/questions
    #  /50775110/how-to-generate-all-combinations-of-lower-and-upper-characters-in-a-word
    results = list(map(''.join, itertools.product(*zip(word.upper(), word.lower()))))
    return results

  def permutateCaseWithSingleCharVariant(self, word):
    results = self.permutateCase(word)
    results.append(word[0].lower())
    results.append(word[0].upper())
    return results

  def permutatePartial(self, word):
    # Using list comprehension, generate all permutations of the word that 
    # start with it's first letter and which are not the supplied word
    results = []
    for numChars in range(2,len(word)+1):
      results = results + [idx for idx in list(map(''.join,\
        itertools.permutations(word, numChars))) if idx != word and idx[0] == word[0]]
    return results


  # Test actions that do not use a count 
  def test_simpleThatDontUseCounts(self):
    rh = ReadoutHelper(default.MAX_DELAY_US)
    for action in simpleActions:
      permutations = self.permutateCaseWithSingleCharVariant(action)
      for variant in permutations:
        # Test each variant without a count - should pass
        self.assertTrue( rh.processSeqParam(variant),\
                         msg='{} should be valid!'.format(variant))

        # Test variant with a count - should fail
        for count in [-1, 0, 1, 2]:
          actionWithCount = str(count)+variant
          self.assertFalse( rh.processSeqParam(actionWithCount),\
                            msg='{} should not be valid!'.format(actionWithCount))

  # Test actions that don't require but can be used with a count
  def test_delaysUsingOptionalCounts(self):
    rh = ReadoutHelper(default.MAX_DELAY_US)
    for delay in delays:
      permutations = self.permutateCaseWithSingleCharVariant(delay)
      for variant in permutations:
        # Test variant without count - should pass
        self.assertTrue( rh.processSeqParam(variant),\
                         msg='{} should be valid!'.format(variant))

        # Test variant with a valid count - should pass
        actionWithCount = str(random.randint(1,500)) + variant
        self.assertTrue( rh.processSeqParam(actionWithCount),\
                         msg='{} should be valid!'.format(actionWithCount))

        # Test variant with an invalid count - should fail
        # Using list slicing shallow copy known invalid values
        badCounts = invalidCounts[:]

        # Append append max+1 to list to check upper limit
        badCounts.append(maxDelayCount[variant[0].lower()] + 1)  # max count + 1
        for count in badCounts:
          actionWithCount = str(count) + variant
          self.assertFalse( rh.processSeqParam(actionWithCount),\
                            msg='{} should be invalid!'.format(actionWithCount))

  # Test actions that don't require but can be used with a count
  def test_readActionsThatRequireCounts(self):
    rh = ReadoutHelper(default.MAX_DELAY_US)
    for action in readActions:
      permutations = self.permutateCaseWithSingleCharVariant(action)
      for variant in permutations:
        # Test variant without count - should fail
        self.assertFalse( rh.processSeqParam(variant),\
                         msg='{} should be invalid!'.format(variant))

        # Test variant with a valid count - should pass
        actionWithCount = str(random.randint(1,500)) + variant
        self.assertTrue( rh.processSeqParam(actionWithCount),\
                         msg='{} should be valid!'.format(actionWithCount))

        # Test variant with an invalid count - should fail
        for count in invalidCounts:
          actionWithCount = str(count) + variant
          self.assertFalse( rh.processSeqParam(actionWithCount),\
                            msg='{} should be invalid!'.format(actionWithCount))

  # Test actions that don't require but can be used with a count
  def test_trigActionsThatRequireCounts(self):
    rh = ReadoutHelper(default.MAX_DELAY_US)
    for action in trigActions:
      permutations = self.permutateCase(action)
      for variant in permutations:
        # Test variant without count - should fail
        self.assertFalse( rh.processSeqParam(variant),\
                         msg='{} should be invalid!'.format(variant))

        # Test variant with a valid count - should pass
        actionWithCount = str(random.randint(1,500)) + variant
        self.assertTrue( rh.processSeqParam(actionWithCount),\
                         msg='{} should be valid!'.format(actionWithCount))

        # Test variant with an invalid count - should fail
        for count in invalidCounts:
          actionWithCount = str(count) + variant
          self.assertFalse( rh.processSeqParam(actionWithCount),\
                            msg='{} should be invalid!'.format(actionWithCount))

  # Test actions that do not use a count.
  # The test uses all partial word combos that start with the first
  # letter of the action. All cases should fail
  def test_simpleActionsThatDontUseCounts_partial(self):
    rh = ReadoutHelper(default.MAX_DELAY_US)
    for action in simpleActions:
      partials = self.permutatePartial(action)
      for partial in partials:
        permutations = self.permutateCase(partial)
        for variant in permutations:
          if variant.lower() == 'tr' or variant.lower() == 'tg': continue

          # Test partial variant without a count - should fail
          self.assertFalse( rh.processSeqParam(variant),\
                            msg='{} should be valid!'.format(variant))

          # Test partial variant with a count - should fail
          for count in [-1, 0, 1]:
            actionWithCount = str(count)+variant
            self.assertFalse( rh.processSeqParam(actionWithCount),\
                              msg='{} should not be valid!'.format(actionWithCount))

  # Test actions that don't require but can be used with a count
  # The test uses all partial word combos that start with the first
  # letter of the action. All cases should fail
  def test_delaysUsingOptionalCounts_partial(self):
    rh = ReadoutHelper(default.MAX_DELAY_US)
    for delay in delays:
      partials = self.permutatePartial(delay)
      for partial in partials:
        permutations = self.permutateCase(partial)
        for variant in permutations:
          # Test partial variant without count - should fail
          self.assertFalse( rh.processSeqParam(variant),\
                            msg='{} should be valid!'.format(variant))

          # Test partial variant with a valid count - should fail
          actionWithCount = str(random.randint(1,maxDelayCount[variant[0].lower()])) + variant
          self.assertFalse( rh.processSeqParam(actionWithCount),\
                            msg='{} should be valid!'.format(actionWithCount))

          # Test partial variant with an invalid count - should fail
          # Using list slicing shallow copy known invalid values
          badCounts = invalidCounts[:]

          # Append append max+1 to list to check upper limit
          badCounts.append(maxDelayCount[variant[0].lower()] + 1)  # max count + 1
          for count in badCounts:
            actionWithCount = str(count) + variant
            self.assertFalse( rh.processSeqParam(actionWithCount),\
                              msg='{} should be invalid!'.format(actionWithCount))

  # Test actions that don't require but can be used with a count
  # The test uses all partial word combos that start with the first
  # letter of the action. All cases should fail
  def test_actionsThatRequireCounts_partial(self):
    rh = ReadoutHelper(default.MAX_DELAY_US)
    for action in readActions:
      partials = self.permutatePartial(action)
      for partial in partials:
        permutations = self.permutateCase(partial)
        for variant in permutations:
          # Test partial variant without count - should fail
          self.assertFalse( rh.processSeqParam(variant),\
                           msg='{} should be invalid!'.format(variant))

          # Test partial variant with a valid count - should fail
          actionWithCount = str(random.randint(1,500)) + variant
          self.assertFalse( rh.processSeqParam(actionWithCount),\
                           msg='{} should be valid!'.format(actionWithCount))

          # Test partial variant with an invalid count - should fail
          for count in invalidCounts:
            actionWithCount = str(count) + variant
            self.assertFalse( rh.processSeqParam(actionWithCount),\
                              msg='{} should be invalid!'.format(actionWithCount))


  def test_readActionWithDelay(self):
    rh = ReadoutHelper(default.MAX_DELAY_US)
    for action in allValidReadActions:
      for delay in allValidDelays:
        actionWithCount = str(random.randint(1,500)) + action
        delayWithCount = str(random.randint(1,maxDelayCount[delay[0].lower()])) + delay

        # # <count><r|l|g|read|line|global><m|u|msec|usec> - should pass
        # sequence = actionWithCount + delay
        # self.assertTrue( rh.processSeqParam(sequence),\
        #                  msg='{} should be valid!'.format(sequence))

        # <count><r|l|g|read|line|global><count><m|u|msec|usec> - should pass
        sequence = actionWithCount + delayWithCount
        self.assertTrue( rh.processSeqParam(sequence),\
                         msg='{} should be valid!'.format(sequence))

        # # <r|l|g|read|line|global><m|u|msec|usec> - should fail, no action count
        # sequence = action + delay
        # self.assertFalse( rh.processSeqParam(sequence),\
        #                  msg='{} should be valid!'.format(sequence))

        # <r|l|g|read|line|global><N><m|u|msec|usec> - should fail, no action count
        sequence = action + delayWithCount
        self.assertFalse( rh.processSeqParam(sequence),\
                         msg='{} should be valid!'.format(sequence))

        # Test with invalid delay count - should fail
        # <r|l|g|read|line|global><INVALID COUNT><m|u|msec|usec>
        # Using list slicing make a shallow copy of the known invalid values 
        # and append (max+1) to list to check detection of exceedint upper limit.
        badCounts = invalidCounts[:]
        badCounts.append(maxDelayCount[delay[0].lower()] + 1)
        for count in badCounts:
          sequence = actionWithCount + str(count) + delay
          self.assertFalse( rh.processSeqParam(sequence),\
                            msg='{} should be invalid!'.format(sequence))

  def test_trigActionWithDelay(self):
    rh = ReadoutHelper(default.MAX_DELAY_US)
    for action in trigActions:
      for delay in allValidDelays:
        actionWithCount = str(random.randint(1,500)) + action
        delayWithCount = str(random.randint(1,maxDelayCount[delay[0].lower()])) + delay

        # # <count><r|l|g|read|line|global><m|u|msec|usec> - should pass
        # sequence = actionWithCount + delay
        # self.assertTrue( rh.processSeqParam(sequence),\
        #                  msg='{} should be valid!'.format(sequence))

        # <count><r|l|g|read|line|global><count><m|u|msec|usec> - should pass
        sequence = actionWithCount + delayWithCount
        self.assertTrue( rh.processSeqParam(sequence),\
                         msg='{} should be valid!'.format(sequence))

        # # <r|l|g|read|line|global><m|u|msec|usec> - should fail, no action count
        # sequence = action + delay
        # self.assertFalse( rh.processSeqParam(sequence),\
        #                  msg='{} should be valid!'.format(sequence))

        # <r|l|g|read|line|global><N><m|u|msec|usec> - should fail, no action count
        sequence = action + delayWithCount
        self.assertFalse( rh.processSeqParam(sequence),\
                         msg='{} should be valid!'.format(sequence))

        # Test with invalid delay count - should fail
        # <r|l|g|read|line|global><INVALID COUNT><m|u|msec|usec>
        # Using list slicing make a shallow copy of the known invalid values 
        # and append (max+1) to list to check detection of exceedint upper limit.
        badCounts = invalidCounts[:]
        badCounts.append(maxDelayCount[delay[0].lower()] + 1)
        for count in badCounts:
          sequence = actionWithCount + str(count) + delay
          self.assertFalse( rh.processSeqParam(sequence),\
                            msg='{} should be invalid!'.format(sequence))

  def test_other(self):
    pass
    # rh = ReadoutHelper(default.MAX_DELAY_US)
    # what = "10trigline"
    # self.assertFalse( rh.processSeqParam(what),\
    #                   msg='{} should be invalid!'.format(what))

if __name__ == '__main__':
  unittest.main()

