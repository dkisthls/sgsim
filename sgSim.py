############################################################################################
# SGServer Simulator 2019-12-18
# Written by: Chris Berst (cberst@nso.edu)
#
# Attempts to mimic the behavior of the StarGrasp controller and attached camera for CSS 
# testing and development purposes.
#
# Developed against Python 3.6.8 and the following packages:
#   astropy==4.0
#   numpy==1.17.4
#
# To determine your Python verision:
#   $ python3 --version
# To determine the versions of installed packages:
#   $ pip3 list
#
# Running:
# With the above described environment setup, just run
#   $ python3 /path/to/sgSim.py
#
# This will first start an instance of fitspipe. By default, the version of fitspipe
# to be run is located in sgDefaults.FITSPIPE_PATH and will started on localhost:9999.
# The constants DEFAULT_FITSPIPE_HOST and DEFAULT_FITSPIPE_PORT define the default
# host and port. Fitspipe will be used by the simulator for writing sim data FITS files.
# If desired, one can telnet in with:
#   $ telnet localhost 9999
#
# Next, sgSim will open a socket to listen on localhost port 8000. This is the command 
# socket. You can telnet in with:
#  telnet localhost 8000
#
#  ... or build up a programatic interface.  For some reason, upon initial client connection 
#  the server sends the client an "IARC\n".  After that, it is an interactive command line interface.
#  It only accepts one connection at a time, and when the client disconnects, it begins waiting for 
#  the next connection with no change in internal state.  For details on the command-line interface
#  see the "dlcam-command-interface" document.  For now, the only output possible is .fits files. 
#  However, with various kinds of testing (including on the real controller) we have shown this to
#  be quite feasible on a ramdisk.  At some point a shared memory interface will be written, but
#  this doesn't exist in the actual controller software yet.
#
#  All output is logged to the console as well as a file: server.log
#
#    As mentioned above, I suggest you setup a tmpfs ramdisk for fits to be written to
#  sudo mount -t tmpfs -o size=1g tmpfs data_tmpfs
#  
###############################################################################################

### Library imports ###

import argparse              # Command line parser (used for set command)
import contextlib            # For closing files when done
import copy                  # For cloning settings dictionary
import logging, sys          # For log generation
import numpy as np           # For generating image array for fits file
import time                  # For generating timing and sleep calls
import os                    # For checking directory exists / creating if not when setting path
import select                # For dealing with non-blocking sockets with a timeout (stop cmd)
import shutil                # For copying fits files
import socket                # For dealing with all things sockets
import subprocess
import shlex

from astropy.io import fits  # For saving fits data
from pathlib import Path     # For dealing with sim data paths/filenames
from threading import Condition, Event, Lock, Thread

### Stargrasp Sim Imports ###
from sgCamera import SGCamera
from sgDefined import sgCommands as sgcmd
from sgDefined import sgResponse as sgresp
from sgDefined import sgStates as sgstate
from sgDefined import sgConstants as const
from sgDefined import sgDefaults as default
from sgReadoutCmdUtil import ReadoutHelper


# Custom action that can be used by the argument parser to check whether a 
# supplied value is in defined bounds:
#  - If minimum != None and maximum != None then check: minimum <= value <= maximum
#  - If minimum == None then check: value <= maximum
#  - If maximum == None then check: value >= minimum
# Note that checks are separated to allow for custom error response based on 
# whether this custom action is being used with a defined minimum, maximum or both.
class HasLimitsAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, const=None, default=None,\
                 type=None, choices=None, required=False, help=None, metavar=None,\
                 minimum=None, maximum=None):
        self.min = minimum
        self.max = maximum
        super(HasLimitsAction, self).__init__(\
            option_strings, dest, nargs, const, default,\
            type, choices, required, help, metavar)

    def __call__(self, parser, namespace, values, option_string=None):
        if not self.min is None and not self.max is None:
            if not values >= self.min or not values <= self.max:
                raise argparse.ArgumentError(self,
                    "value must be in the range {} <= value <= {}".format(self.min,self.max))
        elif self.min is None and not self.max is None:
            if not values <= self.max:
                raise argparse.ArgumentError(self, 
                    "value must be <= {}".format(self.max))
        elif not self.min is None and self.max is None:
            if not values >= self.min:
                raise argparse.ArgumentError(self, 
                    "value must be >= {}".format(self.min))
        setattr(namespace, self.dest, values)

# Custom action that can be used by the argument parser to check whether a
# supplied value is >= 0.
# Note that geq 0 is equivalent to using HasLimitsAction where
# minium=0, maximum=None. This is just clearer (I think).
class GEQZeroAction(argparse.Action):
    def __call__(self, parser, namespace, value, option_string=None):
        if not value >= 0:
            raise argparse.ArgumentError(self, "time value must be >= 0")
        setattr(namespace, self.dest, value)

# Setup parser for command-line arguments
# All arguments are initialized with a default value which may or may not
# wind up being the value actually used, depending on what's defined by
# the command line arguments.
cmdLineParser = argparse.ArgumentParser(
    add_help=True,\
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

cmdLineParser.add_argument("--fread", type=int, 
    action=GEQZeroAction, dest="fread", default=default.T_FREAD,
    help="Time to perform a frame read of the array(ns).")

cmdLineParser.add_argument("--frst", type=int, 
    action=GEQZeroAction, dest="frst", default=default.T_FRST,
    help="Time to reset all FPGAs to state 0(ns).")

cmdLineParser.add_argument("--grst", type=int, 
    action=GEQZeroAction, dest="grst", default=default.T_GRST,
    help="Time to perform a global reset of the array(ns).")

cmdLineParser.add_argument("--lrst", type=int, 
    action=GEQZeroAction, dest="lrst", default=default.T_LRST,
    help="Time to perform a line reset of the array(ns).")

cmdLineParser.add_argument("--seovh", type=int, 
    action=GEQZeroAction, dest="seovh", default=default.T_SEOVH,
    help="Subframe exposure time overhead constant(ns).")

cmdLineParser.add_argument("--sread", type=int, 
    action=GEQZeroAction, dest="sread", default=default.T_SREAD,
    help="Time to perform a subframe integration read of the array(ns).")

cmdLineParser.add_argument("--srint", type=int, 
    action=GEQZeroAction, dest="srint", default=default.T_SRINT,
    help="Subframe row integration time(ns).")

cmdLineParser.add_argument("--tend", type=int, 
    action=GEQZeroAction, dest="tend", default=default.T_TEND,
    help="Time at end of ramp (i.e. delivery latency, other)(ns).")

cmdLineParser.add_argument("--tlat", type=int, 
    action=GEQZeroAction, dest="tlat", default=default.T_TLAT,
    help="Trigger Latency - Delay after timing card sends trigger(ns).")

cmdLineParser.add_argument("--fpbinpath", type=str,
    action="store", dest="fpbinpath", default=default.PATH_TO_FITSPIPE,
    help="Path to fitspipe binaries.")

cmdLineParser.add_argument("--tmpfspath", type=str,
    action="store", dest="tmpfspath", default=default.PATH_TO_TMPFS,
    help="Path to ramdisk tmpfs used for temporary storage of fits files "\
         "created by sgSim and its integrated frame-generator.")

cmdLineParser.add_argument("--tcmd_interrupt", type=float,
    action=GEQZeroAction, dest="tCmdInterrupt", default=default.T_INTERRUPT,
    help="Amount of time, in seconds, it takes sgServer to process an "\
         "interrupt command and respond with INTERRUPTED.")

cmdLineParser.add_argument("--tcmd_readout", type=float, 
    action=GEQZeroAction, dest="tCmdReadout", default=default.T_READOUT,
    help="Amount of time, in seconds, it takes for sgServer to process a "\
         "readout command.")

cmdLineParser.add_argument("--logfile", type=str,
    action="store", dest="logfile", default=default.LOG_FILE,
    help="Sets path and filename for simulator logging.")

cmdLineParser.add_argument("--sgport", type=int,
    action="store", dest="sgport", default=default.SERVER_PORT,
    help="Sets port number for simulator cmd connection.")

cmdLineParser.add_argument("--fpport", type=int,
    action="store", dest="fpport", default=default.FITSPIPE_PORT,
    help="Sets port number for fitspipe.")

# A sequence delay entry is bound by the minimum and maximum values supported.
# This argument is in place in case the maximum delay value is ever increased
# above its current limit defined by const.MAX_DELAY_US.
# If a new value is supplied we need to make sure its at least the minimum value
# Note that the arbument 'minimium' is added by the custom action WithinLimitsAction.
cmdLineParser.add_argument("--maxdelay", type=int,
    action=HasLimitsAction, dest="maxdelay", default=default.MAX_DELAY_US,
    minimum=const.MIN_DELAY_US,     # <-- Custom arg for custom action
    help="The maximum allowed delay time(in us) for a single usec or msec "\
         "sequence entry.")


# Setup parser for the data sub-command
dataParser = argparse.ArgumentParser(add_help=False)

dataParser.add_argument("--frameGenMode", type=str, 
                        choices=const.ALLOWED_FRAMEGEN_MODES,
                        help="sets framegen mode")
dataParser.add_argument("--fixedPixelValue", type=int,
                        help="sets framegen fixed pixel value")
dataParser.add_argument("--path", type=str,
                        help="sets path to sim data files")
dataParser.add_argument("--baseFilename", type=str,
                        help="sets base filename for sim data files")
dataParser.add_argument("--filesPerSet", type=int,
                        help="sets the number of files in the named set")

class SGInterface:
    def __init__(self, cmdLineArgs, primary, camera):
        logging.info('Creating SGInterface...')

        self._state = None                      # 'ready', 'standby', 'active'
        self._state_tlock = Lock()              # thread lock for access to _state
        self.set_curr_state(sgstate.STANDBY)    # System state, retrieved via 'state' command.
        # TEMPORARY
        #self.set_curr_state(sgstate.READY)    # System state, retrieved via 'state' command.
        # END TEMPORARY

        self._cla = cmdLineArgs      # Save command line arguments
        self._primary = primary      # Primary client connection
        self.sgcam = camera

        # Dictionary of StarGrasp commands(keys) and their associated  methods
        self._command_dict = {
            sgcmd.INIT: self.do_init,           # SG
            sgcmd.BEGIN: self.do_begin,         # SG
            sgcmd.END: self.do_end,             # SG
            sgcmd.READOUT: self.do_readout,     # SG
            sgcmd.STANDBY: self.do_standby,     # SG
            sgcmd.STATE: self.do_state,         # SG
            sgcmd.INTERRUPT: self.do_interrupt, # SG, Secondary connection
            sgcmd.SIMTRIG: self.do_simtrig,     # Simulated trigger, secondary connection
            sgcmd.RSIMTRIG: self.do_rsimtrig,   # Simulated trigger, reset framecount, secondary connection
            sgcmd.SHUTDOWN: self.shutdown,      # Sim/CSS
            sgcmd.DATA: self.do_data            # Sim Frame Generator
        }

        self._cmd_argv = []    # Current comand arguments (if any)

        # _cmd_active indicates if a command, which needs to have been preceeded 
        # with a 'begin' command is currently active. The value of _cmd_active is
        # protected with the _cmd_tlock and the state should only be accessed
        # using the set_cmd_active(), set_cmd_inactive() and is_cmd_active() access
        # methods.
        self._is_locked = False       # Camera access has been locked via receipt of a 'begin' command 
        self._cmd_active = False      # Indicates if a command is active        
        self._cmd_active_name = None  # Name of command when _cmd_active == True
        self._cmd_tlock = Lock()      # thread lock for access to _is_locked, _cmd_active, _cmd_active_name


    # Return True: A command is not currently active so the request is satisfied
    # Return False: A command is current active so the request cannot be satisfied
    def set_cmd_active(self, cmd_name, logging_err_msg):
        self._cmd_tlock.acquire()
        success = True
        try:
            if self._cmd_active:     # Is a command already active?
                success = False      # yes - fail setting cmd active
                logging.info('%s, \'%s\' is currently active!'%(logging_err_msg, self._cmd_active_name))
            else:
                self._cmd_active = True
                self._cmd_active_name = cmd_name
            return success
        finally:
            self._cmd_tlock.release()

    def set_cmd_inactive(self):
        self._cmd_tlock.acquire()
        success = True
        try:
            self._cmd_active = False
            self._cmd_active_name = None
            return success
        finally:
            self._cmd_tlock.release()

    def get_curr_state(self):
        self._state_tlock.acquire()
        try:
            return self._state
        finally:
            self._state_tlock.release()

    def set_curr_state(self, value):
        self._state_tlock.acquire()
        try:
            self._state = value
        finally:
            self._state_tlock.release()

    def is_cmd_active(self):
        self._cmd_tlock.acquire()
        try:
            return (self._cmd_active, self._cmd_active_name)
        finally:
            self._cmd_tlock.release()

    def verify_access_is_locked(self):
        self._cmd_tlock.acquire()
        try:
            # NOTE: _is_locked == receipt of a 'begin', it is not a threading lock
            if not self._is_locked:      
                logging.info(
                    "Access is not locked, a \'%s\' has not been received!"%(sgcmd.BEGIN))
                self._primary.sendResponse(sgresp.PREFIX.ERROR, "FAILED")
            return self._is_locked
        finally:
            self._cmd_tlock.release()

    def do_begin(self):                         # lock access
        self._cmd_tlock.acquire()
        try:
            # NOTE: _is_locked == receipt of a 'begin', it is not a threading lock
            if self._is_locked:
                self._primary.sendResponse(
                    sgresp.PREFIX.ERROR, "You already did \'{}\'".format(sgcmd.BEGIN))
            else:
                self._is_locked = True
                self._primary.sendResponse(sgresp.PREFIX.OK, "OK")
        finally:
            self._cmd_tlock.release()

    def do_end(self):                           # unlock access
        self._cmd_tlock.acquire()
        try:
            # NOTE: _is_locked == receipt of a 'begin', it is not a threading lock
            if not self._is_locked:      # cannont 'end' if there was no 'begin'
                self._primary.sendResponse(sgresp.PREFIX.ERROR, "FAILED")
            else:
                self._is_locked = False
                self._primary.sendResponse(sgresp.PREFIX.OK, "OK")
        finally:
            self._cmd_tlock.release()
    
    # Method used to send a '+ request in progress' message back to the client
    # on the primary connection (the connection on which the command was 
    # received). The message is sent on periodically while the command is
    # active.
    # Only active during a sgcmd.READOUT command
    def doInProgressMsg(self):
        t = time.time()
        if t-self.lastInProgressUpdate >= default.IN_PROGRESS_UPDATE_INTERVAL:
            self._primary.sendResponse(sgresp.PREFIX.CONTINUE, "request in progress")
            self.lastInProgressUpdate = t

    # Method used to send a '+ request started' message at the beginning
    # of execution of a 'readout' command. Note that this a point in time
    # after the command and it's parameters are received and validated.
    # Only active during a 'readout' command
    def doStartedMsg(self):
        self.lastInProgressUpdate = time.time()
        self._primary.sendResponse(sgresp.PREFIX.CONTINUE, "request started")

    def do_init(self):                          # Requires 'begin'/'end' access
        if not self.verify_access_is_locked():  # check and respond with error if not
            return
        elif not self.set_cmd_active(sgcmd.INIT, "Camera cannot be initialized"):
            self._primary.sendResponse(sgresp.PREFIX.ERROR, "FAILED")

        try:
            # First response for this command is "+ request started"
            self._primary.sendResponse(sgresp.PREFIX.CONTINUE, "request started")

            # Create and start the thread that will run the 'init' command
            action_thread = Thread(target=self.sgcam.init)
            action_thread.start()

            # Wait for action thread to complete and return.
            # While waiting, send periodic "+ request in progress" responses 
            while action_thread.is_alive():
                action_thread.join(default.IN_PROGRESS_UPDATE_INTERVAL)
                if action_thread.is_alive():    # join timed out, cmd still in progress
                    self._primary.sendResponse(sgresp.PREFIX.CONTINUE, "request in progress")

            if not self.sgcam._initialized:
                logging.info("Camera did not initialize")
                self._primary.sendResponse(sgresp.PREFIX.ERROR, "FAILED")
            else:
                self.set_curr_state(sgstate.READY)
                self._primary.sendResponse(sgresp.PREFIX.OK, "OK")
        finally:
            self.set_cmd_inactive()

    def do_standby(self):                       # Requires 'begin'/'end' access
        if not self.verify_access_is_locked():  # check and respond with error if not
            return
        elif self._state == sgstate.STANDBY:
            self._primary.sendResponse(sgresp.PREFIX.ERROR, "Unable to standby camera from standby state.")
        elif not self.set_cmd_active(sgcmd.STANDBY, "Unable to standby camera"):
            self._primary.sendResponse(sgresp.PREFIX.ERROR, "FAILED")

        try:
            # First response for this command is "+ request started"
            self._primary.sendResponse(sgresp.PREFIX.CONTINUE, "request started")

            # Create and start the thread that will run the 'standby' command
            action_thread = Thread(target=self.sgcam.standby)
            action_thread.start()

            # Wait for action thread to complete and return.
            # While waiting, send periodic "+ request in progress" responses 
            while action_thread.is_alive():
                action_thread.join(default.IN_PROGRESS_UPDATE_INTERVAL)
                if action_thread.is_alive():    # join timed out, cmd still in progress
                    self._primary.sendResponse(sgresp.PREFIX.CONTINUE, "request in progress")

            if self.sgcam._initialized:
                logging.info("Camera did not uninitialize")
                self._primary.sendResponse(sgresp.PREFIX.ERROR, "FAILED")
            else:
                self.set_curr_state(sgstate.STANDBY)
                self._primary.sendResponse(sgresp.PREFIX.OK, "OK")
        finally:
            self.set_cmd_inactive()

    def do_state(self):
        curr_state = self.get_curr_state()

        if curr_state == sgstate.STANDBY:
            logging.info('Camera detector is in standby, system is not operable. \'init\' required.')
        elif curr_state == sgstate.READY:
            logging.info('Camera has been initialised and is ready for use.')
        elif curr_state == sgstate.ACTIVE:
            logging.info('Camera is actively reading out.')
        else:
            logging.info('Need a better log message for state=\'%s\''%(curr_state))
        self._primary.sendResponse(sgresp.PREFIX.OK, curr_state)

    def do_interrupt(self):
        self._primary.sendResponse(sgresp.PREFIX.ERROR, "FAILED")

    def do_readout(self):
        if not self.verify_access_is_locked():  # check and respond with error if not
            return

        # Verify current state is "ready", exclude check for 'active'...
        curr_state = self.get_curr_state()
        if not curr_state == sgstate.READY:
            if curr_state == sgstate.STANDBY:
                self._primary.sendResponse(sgresp.PREFIX.ERROR, 'Camera currently in standby')
            elif not curr_state == sgstate.ACTIVE:
                # This is a catch for later if another state is ever added
                self._primary.sendResponse(sgresp.PREFIX.ERROR, 'Camera in unhandled state \'{}\''.format(curr_state))
            return

        # Now make 'readout' command active command. Method will check for 
        # an already active command and fail if there is one
        if not self.set_cmd_active(sgcmd.READOUT, 'Cannot execute \'readout\''):
            self._primary.sendResponse(sgresp.PREFIX.ERROR, "FAILED")
            return

        try: 
            t0 = time.time()

            readoutCmd = ReadoutHelper(self._cla.maxdelay)

            # Check to make sure arguments were passed
            if len(self._cmd_argv) == 0:
                logging.info("No readout options specified, doing nothing")
                self._primary.sendResponse(sgresp.PREFIX.ERROR, "FAILED")
                return
            
            # Create deep copy of setting in case any readout sub-commands run 
            # into valdiation errors
            oldSettings = copy.deepcopy(self.sgcam.settings)

            (success1, errMsg1, mode, sequenceList, seqFrameCount, numFrames, subFramePeriod) = \
                readoutCmd.processArgs(self._cmd_argv)

            if success1:
                self.sgcam.settings['mode'] = mode
                (success2, errMsg2, tGlobalReset_s, tLineReset_s, tFrameRead_s, tSubFrameRead_s, tSubFrameExpTime_s, tTotalSequenceTime_s) = \
                    self.sgcam.figure_timing(sequenceList, subFramePeriod)

            # If there are any errors, reset to old settings and exit
            if not success1 or not success2:
                self.sgcam.settings = oldSettings
                if not success1:
                    logging.info(errMsg1)
                else:
                    logging.info(errMsg2)
                self._primary.sendResponse(sgresp.PREFIX.ERROR, "FAILED")
                return

            # Log the sequence we'll execute on receipt of a sgcmd.SIMTRIG
            # or sgcmd.RSIMTRIG command.
            if const.DEBUG_LOG_SEQUENCE_ENTRIES:
               for s in sequenceList:
                   logging.info(s)
  
            logging.info( "APPLYING: readout "+" ".join(self._cmd_argv) )

            # save this readout sequence counts/timing values...
            self.sgcam.settings['sequenceList'] = sequenceList
            self.sgcam.settings['seqFrameCount'] = seqFrameCount
            self.sgcam.settings['subframe_period'] = subFramePeriod
            self.sgcam.settings['nframe'] = numFrames
            self.sgcam.timing['tFrameRead_s'] = tFrameRead_s
            self.sgcam.timing['tGlobalReset_s'] = tGlobalReset_s
            self.sgcam.timing['tLineReset_s'] = tLineReset_s
            self.sgcam.timing['tSubFrameRead_s'] = tSubFrameRead_s
            self.sgcam.timing['tSubFrameExpTime_s'] = tSubFrameExpTime_s
            self.sgcam.timing['tTotalSequenceTime_s'] = tTotalSequenceTime_s

            # Calculate remaining time for this command and wait
            remainingCmdTime = self._cla.tCmdReadout - (time.time()-t0)
            if remainingCmdTime >= 0:
                time.sleep(remainingCmdTime)

            self.set_curr_state( sgstate.ACTIVE )

            # First response for this command is "+ request started"
            self._primary.sendResponse(sgresp.PREFIX.CONTINUE, "request started")

            # Create and start the thread that will run the 'readout' command
            status = [None]  # Mutable object for return status from thread
            statusIdx = 0    # Associated index for executeReadout to place return status
            action_thread = Thread(target=self.sgcam.executeReadout, args=(status,statusIdx))
            action_thread.start()

            # Wait for action thread to complete and return.
            # While waiting, send periodic "+ request in progress" responses 
            while action_thread.is_alive():
                action_thread.join(default.IN_PROGRESS_UPDATE_INTERVAL)
                if action_thread.is_alive():    # join timed out, cmd still in progress
                    self._primary.sendResponse(sgresp.PREFIX.CONTINUE, "request in progress")

            if status[statusIdx] == 'Complete':
                self._primary.sendResponse(sgresp.PREFIX.OK, "OK")
            elif status[statusIdx] == 'Interrupted':
                # Simulate how long it takes for SGServer to stop.
                time.sleep(self._cla.tCmdInterrupt)

                # Now send 'INTERRUPTED' response.
                self._primary.sendResponse(sgresp.PREFIX.OK, 
                  "INTERRUPTED {}".format(self.sgcam.info['total_writes']))
            else:
                self._primary.sendResponse(sgresp.PREFIX.ERROR, "FAILED")

            self.set_curr_state(sgstate.READY)

        finally:
            self.set_cmd_inactive()


    #--------------------- Simulator Only Command Handlers ---------------------
    # These commands do not currently use a thread lock
    def do_data(self):
       
        # Check to make sure arguments were passed
        if len(self._cmd_argv) == 0:
            logging.info("No data options specified, doing nothing")
            self._primary.sendResponse(sgresp.PREFIX.ERROR, 
                "ERROR: No data options specified")
            return
        
        # Run arguments through argparse instance we set up at beginning
        # It does basic validation, so catch any errors it throws up
        # ( More detailed errors go to stdout, user gets general error )
        try:
            args = dataParser.parse_args(self._cmd_argv)
        except SystemExit:
            logging.info("Bad data options")
            self._primary.sendResponse(sgresp.PREFIX.ERROR,
                "ERROR: Bad data options")
            return
        
        # If it doesn't throw up an error, argparse instance will split
        # any parsed arguments into a nice format in args
        
        # Create deep copy of settings in case any set sub-commands run into valdiation errors
        oldData = copy.deepcopy(self.sgcam.data)
        
        # Track if any data errors occured
        dataError = False
        
        # Track data command changes for logging
        data_results = []  
        
        # Validate all inputs more rigorously before changing anything
        if args.frameGenMode != None:
            argValidated = self.sgcam.dataFrameGenMode(args.frameGenMode)
            data_results.append( str("frameGen mode: %s" % args.frameGenMode) )
            if not argValidated:
                self._primary.sendResponse(sgresp.PREFIX.ERROR, 
                    "ERROR: Problem setting frameGen mode, %s" % self.sgcam.data['errorReason'])
                dataError = True

        if not dataError and args.fixedPixelValue != None:
            argValidated = self.sgcam.dataFixedPixelValue(args.fixedPixelValue)
            data_results.append( str("frameGen fixedPixelValue: %04d" % (args.fixedPixelValue) ) )
            if not argValidated:
                self._primary.sendResponse(sgresp.PREFIX.ERROR, 
                    "ERROR: Bad fixedPixelValue, %s" % self.sgcam.data['errorReason'] )
                dataError = True

        if not dataError and args.path != None:
            argValidated = self.sgcam.dataPath(args.path)
            data_results.append( str("sim data path: %s" % (args.path) ) )
            if not argValidated:
                self._primary.sendResponse(sgresp.PREFIX.ERROR, 
                    "ERROR: Bad path, %s" % self.sgcam.data['errorReason'] )
                dataError = True

        if not dataError and args.baseFilename != None:
            argValidated = self.sgcam.dataBaseFilename(args.baseFilename)
            data_results.append( str("base filename: %s" % (args.baseFilename) ) )
            if not argValidated:
                self._primary.sendResponse(sgresp.PREFIX.ERROR, 
                    "ERROR: Bad baseFilename, %s" % self.sgcam.data['errorReason'] )
                dataError = True

        if not dataError and args.filesPerSet != None:
            argValidated = self.sgcam.dataFilesPerSet(args.filesPerSet)
            data_results.append( str("files-per-set: %s" % (args.filesPerSet) ) )
            if not argValidated:
                self._primary.sendResponse(sgresp.PREFIX.ERROR, 
                    "ERROR: Bad filesPerSet, %s" % self.sgcam.data['errorReason'] )
                dataError = True

        if not dataError and self.sgcam.data['frameGenMode'] == "Off":
            success = False
            path = self.sgcam.data['path']
            baseFilename = self.sgcam.data['baseFilename']
            filesPerSet = self.sgcam.data['filesPerSet']
            if not path:
                self.sgcam.data['errorReason'] = "sim data 'path' is not defined"
            elif not baseFilename:
                self.sgcam.data['errorReason'] = "sim data 'baseFilename' is not defined"
            elif filesPerSet <= 0:
                self.sgcam.data['errorReason'] = "sim data 'filesPerSet' is not defined"
            else:
                success = True

            if not success:
                self._primary.sendResponse(sgresp.PREFIX.ERROR, 
                    "ERROR: frameGenMode=\"Off\" but %s" % self.sgcam.data['errorReason'] )
                dataError = True
            else:
                # Build up filenames and test if file exists and is readable...
                numFiles = int(self.sgcam.data['filesPerSet'])
                for i in range(numFiles): 
                    filename = self.sgcam.buildSimDataFilename(i, "fits")
                    p = Path(self.sgcam.data['path']) / filename

                    if not p.exists():
                        self.sgcam.data['errorReason'] = "'{}' does not exist".format(p)
                        self._primary.sendResponse(sgresp.PREFIX.ERROR, 
                            "ERROR: frameGenMode=Off but %s" % self.sgcam.data['errorReason'] )
                        dataError = True
                        break

                    if not os.access(str(p), os.R_OK):
                        self.sgcam.data['errorReason'] = "'{}' is not readable".format(p)
                        self._primary.sendResponse(sgresp.PREFIX.ERROR, 
                            "ERROR: frameGenMode=Off but %s" % self.sgcam.data['errorReason'] )
                        dataError = True
                        break

        # If there are any data errors, reset to old data and exit
        if dataError:
            self.sgcam.data = oldData
            return
        
        logging.info( "APPLYING: "+", ".join(data_results) )

        self._primary.sendResponse(sgresp.PREFIX.OK, "OK")
    
    def do_simtrig(self):
        # A sgcmd.SIMTRIG command serves as a "soft" hardware trigger. It is 
        # only valid following receipt of a sgcmd.BEGIN followed by a 
        # sgcmd.READOUT command and is looked for during 'readout sequence' 
        # processing by the method SGCamera.trigOrInterrupt() during 
        # SGCamera.executeSequence(). 
        # If we get to this point then a sgcmd.SIMTRIG command was recieved but
        # a sgcmd.READOUT is not active. Simply ignore it.
        pass

    def do_rsimtrig(self):
        # This command is used to reset source file index to 0 then execute 
        # sgcmd.SIMTRIG.
        # As discussed above, receipt of an sgcmd.RSIMTRIG is handled by 
        # SGCamera.trigOrInterrupt() and if received here it can simply be 
        # ignored.
        pass

    # 'shutdown' is not a Stargrasp command. It is provided solely to allow for a
    # clean(ish) shutdown of the simulator...
    def shutdown(self):
        self._cmd_tlock.acquire()
        try:
            self.sgcam.fitspipe.shutdown()

            logging.info("Waiting for client disconnect...")
            self._primary.sendResponse(sgresp.PREFIX.OK, "OK")
            
            # Go into a readout loop, but respond to any request with an error
            # shutdown SGServer on client disconnect.
            done = False
            while not done:
                try:
                    # Wait for a command
                    sockMsg = self._primary.recv()

                    logging.info("Command received while waiting for shutdown, sending error")
                    self._primary.sendResponse(sgresp.PREFIX.ERROR,
                        "ERROR: SGServer is shutting down, disconnect to complete")
                except RuntimeError as e:
                    if e.args[0] == "SOCK_BROKEN":
                        logging.info("Connection closed by foreign host, completing shut down...")
                        done = True
                    else:
                        raise e
        finally:
            self._cmd_tlock.release()
    
    def parseCMD(self, cmd_string):
        cmd_name = None     # The command to run
        cmd_func = None     # The function associated with the command

        # First strip away possible comment
        where = cmd_string.find(const.START_OF_COMMENT)
        if where != -1:
            cmd_string = cmd_string[:where]

        cmdSplit = cmd_string.split()
        if len(cmdSplit) > 0:
            cmd_name = cmdSplit[0]
            self._cmd_argv = cmdSplit[1:]
            if cmd_name in self._command_dict:
                cmd_func = self._command_dict.get(cmd_name)
            else:
                errMsg = 'Unrecongnized command \'{}\'!'.format(cmd_name)
                logging.info(errMsg)
                self._primary.sendResponse(sgresp.PREFIX.ERROR, errMsg)

        return (cmd_name, cmd_func)

class SockHandler:
    def __init__(self, svrSocket, who, bufsize):
        # Create a socket of the standard TCP streaming type
        self.sock = svrSocket
        self.who = who
        self._bufsize = bufsize
        self.hasConnection = False
        self.connection = None

    def waitForConnect(self):
        # Wait for connection
        logging.info('Waiting for {} connection...'.format(self.who))
        self.hasConnection = False
        (self.connection, self.address) = self.sock.accept()
        self.hasConnection = True
        logging.info('Received {} connection, socket info: {} {}'.format(
            self.who, self.connection, self.address))
        
    def sendResponse(self, prefix, msg):
        # prefix - A sgresp.PREFIX enum. Used to index the sgresp.PREFIX_STR array
        #          to retrieve the prefix character.
        # msg - A string containing a message for the response
        msg = str("%s%s"%(sgresp.PREFIX_STR[prefix.value], msg))
        
        self.send(msg)
    
    def send(self, msg):
        # Tack newline onto messages
        msg = '{}\n'.format(msg)
        logging.info('RESPONSE: {}'.format(repr(msg)))
        
        # Encode the message and send
        sent = self.connection.send(str.encode(msg))
        
        if sent == 0:
            raise RuntimeError('SOCK_BROKEN')

    def recv(self):
        # Wait for something to be received on this socket
        data = self.connection.recv(self._bufsize)
        
        # Catch socket broken with empty byte string
        if data == b'':
            raise RuntimeError('SOCK_BROKEN')

        msg = bytes.decode(data)
        logging.info('RECEIVED: {}'.format(repr(msg)))

        if const.DEBUG_SOCKET:
            ascii_values = []
            for character in msg:
                ascii_values.append(ord(character))
            logging.info('Connection: {}\n{}Command: {}\n{}ASCII Char Codes {}'.format(
                self.who, (' '*27), repr(msg), (' '*27), ascii_values))
        
        return msg

class FitspipeHandler:
    class FrameGenHdu:
        def __init__(self, hdu, infoStr):
            self._hdu = hdu
            self._loginfo = infoStr

    class SimFile:
        def __init__(self, fileName, infoStr):
            self._filename = fileName
            self._loginfo = infoStr

    def __init__(self, host, port, path):
        self.host = host
        self.port = port
        self.path = path

        # If present, replace ATST environment var with its expanded value
        if path.find("$ATST") != -1:
            self.path = path.replace("$ATST", os.environ["ATST"])
        
        # Check to make sure this is a legitamate path, signal failure if not
        p = Path(self.path)
        if not p.exists():
            errMsg = "Path to fitspipe binaries does not exist: '{}'!".format(self.path)
            logging.info(errMsg)
            raise RuntimeError(errMsg)

        if not default.DEBUG_FITSPIPE:
          daemon = 'daemon={}'.format(default.FITSPIPE_DAEMON)
          verbose = ''
        else:
          # verbose (debug) outpout only works when fitspipe is NOT run as a daemon
          daemon = 'daemon=false'
          verbose = 'verbose={:d}'.format(default.FITSPIPE_DEBUG_VERBOSITY)

        command_line = '{}{} {} {} {}'.format(self.path, default.FITSPIPE_BIN_NAME, str(self.port), daemon, verbose)
        logging.info('Starting fitspipe server on {} using: {}'.format(self.host, command_line))

        # Start the fitspipe server
        args = shlex.split(command_line)
        subprocess.Popen(args)

        # Create a socket of the standard TCP streaming type
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Connect to fitspipe server that was just started...
        logging.info("Connecting to fitspipe server on %s:%d..."%(self.host,self.port))
        self.connect()

        cmd = 'put feed=sgSim'
        self.sendCmd( cmd )

        resp = self.readResp()
        prefix = self.get_resp_prefix(resp)
        if not prefix == sgresp.PREFIX.OK:
            raise RuntimeError("FITSPIPE ERROR: " + resp)

        # Fitspipe write thread queues, locks, and condition vars
        self._file_queue = []      # Sim FITS files already on disc
        self._hdu_queue = []       # FrameGen FITS HDUs created on the fly
        self._queue_lock = Lock()  # Lock for file/hdu queues

        self._terminate_fitspipe_writer = False    # Termination flag
        self._fitspipe_writer_thread = None        # The write thread
        self._item_ready_lock = Lock()             # lock for Condition
        self._cv_item_ready = Condition(self._item_ready_lock)

        # Create and start the fitspipe writer thread
        thread_name = 'fitspipe_writer'
        started_event = Event()
        self._fitspipe_writer_thread = Thread(target=self.fitspipe_writer, 
                                              name=thread_name,
                                              args=(thread_name, started_event))
        self._fitspipe_writer_thread.start()
        if not started_event.wait(const.THREAD_START_TIMEOUT):
            raise RuntimeError(
                'FITSPIPE ERROR: Timeout waiting {:03f}sec for {}} thread to start!'.format(
                    thread_name, const.THREAD_START_TIMEOUT) )
        logging.info('{} thread running...'.format(thread_name))


    def connect(self):
        connected = False
        for t in range (1, default.FITSPIPE_CONNECT_TIMEOUT):
            # Attempt to connect to the fitspipe server...
            if self.sock.connect_ex((self.host,self.port)) == 0:
                connected = True
                break
            else:
                time.sleep(1)
                    
        if connected:
            logging.info("SUCCESS, connected to fitspipe server on %s:%d!"%(self.host,self.port))
        else:
            logging.info("FAILURE, tried for %d seconds, cannot connect to fitspipe "
                         "server on %s:%d!"%(default.FITSPIPE_CONNECT_TIMEOUT,self.host,self.port))
            raise RuntimeError()

    def putFile(self, filename, seqInfoStr):
        infoStr = 'Writing to fitspipe: {}, filename={}'.format(seqInfoStr,filename)
        with self._queue_lock:
            self._file_queue.append( self.SimFile(filename, infoStr) )
        with self._item_ready_lock:
            self._cv_item_ready.notify()
    
    def putHdu(self, hdu, seqInfoStr, xtraInfoStr):
        infoStr = 'Writing to fitspipe: {}, hdu from {}'.format(seqInfoStr, xtraInfoStr)
        with self._queue_lock:
            self._hdu_queue.append( self.FrameGenHdu(hdu, infoStr) )
        with self._item_ready_lock:
            self._cv_item_ready.notify()

    # Parses the prefix on the head of a line of response from the server. 
    # Returns an entry from the PREFIX enumeration if that status indication was
    # found, otherwise None if there was nothing sensible.
    def get_resp_prefix(self,line):
        for prefix in sgresp.PREFIX:
            if line[:2] == sgresp.PREFIX_STR[prefix.value]:
                return prefix

        # Nothing found.
        return None

    def readResp(self):
        # Wait for something to be received on this socket
        data = self.sock.recv(const.PRIMARY_BUF_SIZE)
        
        # Catch socket broken with empty byte string
        if data == b'':
            raise RuntimeError("SOCK_BROKEN")
        
        resp = bytes.decode(data)
        return resp

    def sendCmd(self, cmd):
        # Tack newline onto messages
        msg = str("%s\n" % (cmd))
        
        # Encode the message and send
        sent = self.sock.send(str.encode(msg))
        
        if sent == 0:
            raise RuntimeError("SOCK_BROKEN")

    def shutdown(self):
        if self._fitspipe_writer_thread.is_alive():
            try: 
                logging.info('Terminating _fitspipe_writer_thread...')
                self._terminate_fitspipe_writer = True   # Set terminate flag
                with self._cv_item_ready:
                    self._cv_item_ready.notify()         # Notify thread
                self._fitspipe_writer_thread.join()      # Rejoin
                logging.info('_fitspipe_writer_thread terminated')
            except Exception as ex:
                errMsg = 'Exception during _fitspipe_writer_thread.join(1) - {}'.format(ex)
                logging.info(errMsg)

        logging.info("")
        logging.info("Sending \'%s\' to fitspipe..."%(sgcmd.SHUTDOWN))
        self.sock.send(sgcmd.SHUTDOWN.encode())

        logging.info("Closing fitspipe socket...")
        self.sock.close()

        logging.info("Fitspipe shutdown complete!")

    def fitspipe_writer(self, name, started_event):
        logging.info('{} thread starting...'.format(name))
        started_event.set()

        while not self._terminate_fitspipe_writer:
            with self._cv_item_ready:
                self._cv_item_ready.wait()

            if self._terminate_fitspipe_writer: continue

            done = False
            while not done:
                isHdu = False
                isFile = False
                with self._queue_lock:
                    if len(self._hdu_queue) > 0:
                        item_to_write = self._hdu_queue.pop(0)
                        isHdu = True
                    elif len(self._file_queue) > 0:
                        item_to_write = self._file_queue.pop(0)
                        isFile = True
                    else:
                        done = True
                        continue

                logging.info( item_to_write._loginfo )  # Log the info string

                if const.DEBUG_FITSPIPE_THREAD: tWriteStart = time.time()

                # Do the write, either with a put to FitsPipe of and HDR
                # from the frame-generatore(isHdu) or a sim file (isFile)
                if isHdu:
                    with contextlib.closing(self.sock.makefile('wb')) as f:
                        item_to_write._hdu.writeto( f )
                else:  # isFile
                    with contextlib.closing(open(item_to_write._filename,'rb')) as f:
                       numBytesSent = self.sock.sendfile(f, 0)

                if const.DEBUG_FITSPIPE_THREAD:
                    logging.info("Time to write fits: %.03fms"%\
                        ((time.time() - tWriteStart) * const.MILLISECONDS_PER_SECOND))


### Main program runtime ###
def initialize_logger(cmdLineArgs):
    # Set up our logs (These need to be initialized before we define our classes below)
    logging.basicConfig(filename=cmdLineArgs.logfile,
                        format='[%(asctime)s.%(msecs)03dZ] %(message)s', 
                        datefmt='%Y-%m-%dT%H:%M:%S',
                        level=logging.DEBUG)
    logger = logging.getLogger()
    logging.Formatter(fmt='[%(asctime)s.%(msecs)03d] %(message)s', datefmt='%Y-%m-%dT%H:%M:%S')
    logging.Formatter.converter = time.gmtime

    # Also output to console for now
    consoleOut = logging.StreamHandler(sys.stdout)
    consoleOut.setLevel(logging.INFO)

def log_startup_info(cmdLineArgs):
    # Output that we're getting started
    logging.info('Starting new sgSim run')

    # Log the values being used that came either from the default values established
    # with the command line parser or from the actual command line...
    logging.info('Using frst (time for FPGA reset)                   : {}ns'.format(cmdLineArgs.frst))
    logging.info('Using fread (time for Frame read)                  : {}ns'.format(cmdLineArgs.fread))
    logging.info('Using grst (time for Global reset)                 : {}ns'.format(cmdLineArgs.grst))
    logging.info('Using lrst (time for Line reset)                   : {}ns'.format(cmdLineArgs.lrst))
    logging.info('Using seovh (subframe exposure time overhead)      : {}ns'.format(cmdLineArgs.seovh))
    logging.info('Using sread (time for subframe read)               : {}ns'.format(cmdLineArgs.sread))
    logging.info('Using srint (subframe row integration time)        : {}ns'.format(cmdLineArgs.srint))
    logging.info('Using tend (Time at end of ramp)                   : {}ns'.format(cmdLineArgs.tend))
    logging.info('Using tlat (Trigger Latency time)                  : {}ns'.format(cmdLineArgs.tlat))
    logging.info('Using maxdelay (Max delay usec/msec sequence entry): {}us'.format(cmdLineArgs.maxdelay))
    logging.info('Using tCmdInterrupt (Time for a interrupt command) : {}s'.format(cmdLineArgs.tCmdInterrupt))
    logging.info('Using tCmdReadout (Time for a readout command)     : {}s'.format(cmdLineArgs.tCmdReadout))
    logging.info('Using tmpfs path       : {}'.format(cmdLineArgs.tmpfspath))
    logging.info('Using fitspipe bin path: {}'.format(cmdLineArgs.fpbinpath))
    logging.info('Using fitspipe binary  : {}'.format(default.FITSPIPE_BIN_NAME))
    logging.info('Using logfile          : {}'.format(cmdLineArgs.logfile))
    logging.info('Using cmd port #       : {}'.format(cmdLineArgs.sgport))
    logging.info('Using fitspipe port #  : {}'.format(cmdLineArgs.fpport))

def main():
    # Parse command line arguments
    cmdLineArgs = cmdLineParser.parse_args()

    if const.DEBUG_PRINT_CMDLINE_ARGS:
      for arg in vars(cmdLineArgs):
          print(arg, getattr(cmdLineArgs, arg))

    initialize_logger(cmdLineArgs)

    # Check to make sure the tmpfs path is legitamate. Signal failure if not,
    # otherwise assure it ends in a '/'.
    p = Path(cmdLineArgs.tmpfspath)
    if not p.exists():
        errMsg = 'Directory \'{}\' does not exist, please create as a 1G tmpfs ramdisk!'\
                 .format(cmdLineArgs.tmpfspath)
        logging.info(errMsg)
        cmdLineParser.error(errMsg)
    elif cmdLineArgs.tmpfspath[-1] != '/':
        cmdLineArgs.tmpfspath += '/'

    log_startup_info(cmdLineArgs)

    # Create an instance of our socket connection handler
    # Create a socket of the standard TCP streaming type
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        # Set option which tells the kernel to reuse a local socket in 
        # TIME_WAIT state, without waiting for its natural timeout to expire
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # Bind to localhost:<sgport>
        sock.bind((default.SERVER_HOST, cmdLineArgs.sgport))

        # Set max connections to 1 and listen
        sock.listen(5)

        # Create an instance of our fitspipe connection 
        try:
            fitspipe = FitspipeHandler(default.FITSPIPE_HOST,\
                                       cmdLineArgs.fpport,\
                                       cmdLineArgs.fpbinpath)
        except RuntimeError as e:
            raise e

        primary = SockHandler(sock, 'primary', const.PRIMARY_BUF_SIZE)
        secondary = SockHandler(sock, 'secondary', const.SECONDARY_BUF_SIZE)

        # Create an instance of the camera handing it the command line 
        # arguments, the secondary socket connection, and fitspipe.
        sg_camera = SGCamera(cmdLineArgs, secondary, fitspipe)

        # Create an instance of the camera command interface handing it the 
        # command line arguments, the primary socket connection, and the camera.
        sg_interface = SGInterface(cmdLineArgs, primary, sg_camera)

        primary.waitForConnect()
        secondary.waitForConnect()

        shutdown = False
        while not shutdown:
            try:
                # Wait for a command
                sockMsg = primary.recv()

                # Pass received string to SGInterface command parser which will
                # return the command name and associated processing function.
                (cmd, cmd_func) = sg_interface.parseCMD(sockMsg)

                if cmd == sgcmd.SHUTDOWN:
                    sg_interface.shutdown()
                    shutdown = True
                elif not cmd is None:
                    # thread = Thread(target=cmd_func, name=cmd)
                    # thread.start()    # blindly start and let run to completion
        # TEMPORARY
                    cmd_func()
        # END TEMPORARY

            except RuntimeError as e:
                if e.args[0] == 'SOCK_BROKEN' and not shutdown:
                    logging.info('Socket broken, waiting for reconnect...')
                    primary.waitForConnect()
                elif not shutdown:
                    raise e

    logging.info('sgSim Shutdown Complete!')

if __name__ == '__main__':
    main()
