### Python Library imports ###
import argparse              # Command line parser (used for set command)
import re                    # For regular expressions parsing readout parameters
import string                # For whitespace checks in sequence
from enum import Enum, auto  # Used for status indicators, sequence actions

### Stargrasp Sim Imports ###
from sgDefined import sgConstants as const
from sgDefined import sgDefaults as default

# Scaling values for both long and short forms of a delay action to convert
# counts to microseconds.
DELAY_SCALER = {'m': const.ONE_THOUSAND, 'msec': const.ONE_THOUSAND,\
                'u': 1, 'usec': 1}

SEQ_SEPARATOR = ','          # Separator char between sequence entries

### Class definitions ###
class SequenceAction(Enum):
    TRIG     = auto()    # stand alone 't' or 'trigger' entry
    LINE     = auto()    # {t|trig}<line>
    GLOBAL   = auto()    # {t|trig}<global>
    READ     = auto()    # {t|trig}<read>
    SUBFRAME = auto()    # {t|trig|<subframe>}
    DELAY    = auto()    

class SequenceEntry:
    def __init__(self, action, delay, count, isTriggered):
        self.action      = action       # one of trig,line,read,global,delay
        self.delay       = delay        # if defined, a delay time (in microseconds)
        self.count       = count        # Number of times to repeat this entry
        self.isTriggered = isTriggered  # Boolean flag that this entry is triggered

    def __repr__(self):
        return f'SequenceEntry[{vars(self)}]'


### Readout Command Argument Parser ###
### Extend argparse.ArgumentParser so that the error() method can be 
### overloaded. Argparse is being used to parse command errors that 
### don't come from the command line. If an error occurs we want to
### capture the error message and return it rather than have it go to
### stdout.
class ReadoutParserError(Exception): pass

class ReadoutCommandParser(argparse.ArgumentParser):
  def error(self, message):
    raise ReadoutParserError(message)

readoutParser = ReadoutCommandParser(add_help=False, usage='', prefix_chars='-',
                                     prog='ReadoutCommandParser')
readoutParser.add_argument('--seq', required=True)
readoutParser.add_argument('--nframe', type=int)
readoutParser.add_argument('--subframe_period', type=int)

class ReadoutHelper:
    def __init__(self, maxDelay_us = default.MAX_DELAY_US):
        self.maxDelay_us = maxDelay_us
        self.errMsg = ''
        self.args = []
        self.sequence = []
        self.numFrames = 0
        self.totalDelay = 0           # Total delay time in microseconds
        self.numGlobalResets = 0      # Total number of global resets in one pass
        self.numLineResets = 0        # Total number of line resets in one pass
        self.numReads = 0             # Total number of reads in one pass
        self._num_subframes = 0       # Total number of subframe reads in one pass
        self.seqFrameCount = 0        # Total number of frames in one pass
        self._subframe_period = 0     # Total number of lines for subframe integration
        self._mode = None             # Mode determined by readout sequence

    def parseArgs(self, argv):
        self.errMsg = "OK"
        # Run arguments through argparse instance we set up at beginning
        # It does basic validation, so catch any errors it throws up
        # ( More detailed errors go to stdout, user gets general error )
        try:
            # We're using argparse. The stargrasp interface documentation
            # lists 'nframe' and 'subframe_period' as optional parameters. 
            # argparse can use the '-' prefix for non positional, optional 
            # parameters but the stargrasp interface doesn't define the prefix '-'. 
            # So, we'll play a trick:
            #  - In the definition of the readoutParser we define 'seq', 'nframe',
            #    and 'subframe_period' as non-positional parameters using the '--' 
            #    prefix.
            #  - We define '--seq' as being required while '--nframe' and 
            #    'subframe_period' will have the default behaviour of being optional.
            #  - Since stargrasp doesn't define the '--' prefix but we want its
            #    behaviour, 'seq', 'nframe', and 'subframe_period' in the argument 
            #    list are replaced with '--seq', '--nframe', and '--subframe_period' 
            #    before being sent to the parser.
            #  - Following replacement, if the modified arguments only contain 
            #    '--seq=' then no error will be generated since '--nframe' and 
            #    '--subframe_period' are optional as far as readoutParser is 
            #    concerned.
            #  - If the modified arguments contain '--nframe' or '--subframe_period'
            #    then they will get processed as required.
            modifiedArgv = []
            for i in range(0, len(argv)):
                modifiedArgv.append(argv[i].lower())
                if modifiedArgv[i].find('seq') != -1:
                    modifiedArgv[i] = '--' + modifiedArgv[i]
                elif modifiedArgv[i].find('nframe') != -1:
                    modifiedArgv[i] = '--' + modifiedArgv[i]
                elif modifiedArgv[i].find('subframe_period') != -1:
                    modifiedArgv[i] = '--' + modifiedArgv[i]

            self.args = readoutParser.parse_args(modifiedArgv)

        except ReadoutParserError as rpe:
            # ReadoutParserError just returns the error message of the failure. 
            # Since we "played a trick" with use of '--' then if it's present in
            # the error message we need to remove it so as to not confuse the user.
            self.errMsg = 'Bad \'readout\' command, ' + str(rpe)
            self.errMsg = self.errMsg.replace('--', '')
            return False;

        except SystemExit:
          self.errMsg = 'Bad readout options'
          return False

        # A couple quick checks for parameters defined with only 'param=' 
        # and no value following the equal sign.
        if self.args.seq == '':
            self.errMsg = 'Readout parameter \'seq=\' value is missing!'
            return False
        elif not self.args.nframe == None and self.args.nframe == '':
            self.errMsg = 'Readout parameter \'nframe=\' value is missing!'
            return False
        elif not self.args.subframe_period == None and self.args.subframe_period == '':
            self.errMsg = 'Readout parameter \'subframe_period=\' value is missing!'
            return False

        return True

    def contains_whitespace(self, s):
        return True in [c in s for c in string.whitespace]

    def processSeqParam(self, seqParamValue):
        # Decompose sequence. They are of the form:
        #   <count>{type}{delay}{units}
        #   where: <xxx> == required, {xxx} == optional
        #   and where:
        #     <count> : integer >0
        #     <type>  : l*ine, g*lobal, r*ead, s*ubframe
        #     {delay} : integer delay
        #     {units} : m*sec, u*sec
        #   NOTE: Characters following '*' are optional
        #   NOTE: Text is case insensitive
        self.errMsg = "OK"
        seqFrameCount = 0
        self.sequence = []

        # Check for sequence limits as imposed by StarGrasp and control software
        if self.contains_whitespace(seqParamValue):
            self.errMsg = 'Invalid readout sequence, ' \
                'sequence string cannot contain whitespace!'
            return False

        # Check that sequence string does not end with the separator (',")
        elif seqParamValue.endswith(SEQ_SEPARATOR):
            self.errMsg = 'Invalid readout sequence, ' \
                'sequence string cannot end with a \'{}\'!'.format(SEQ_SEPARATOR)
            return False


        elif len(seqParamValue) > const.MAX_SEQ_LEN:
            self.errMsg = 'Invalid readout sequence, ' \
                'sequence string length={} - length must be <= {}!'.format(
                    len(seqParamValue), const.MAX_SEQ_LEN)
            return False

        entries = seqParamValue.split(SEQ_SEPARATOR)

        if len(entries) > const.MAX_SEQ_ENTRIES:
            self.errMsg = 'Invalid readout sequence, ' \
                'number of sequence entries={} - number of entries must be <= {}!'.format(
                    len(entries), const.MAX_SEQ_ENTRIES)
            return False

        for entry in entries:
            frameRepeatCount = 0
            lcEntry = entry.lower()     # lce == lower case entry

            # Split the entry on numeric boundaries. Entry should
            # one of the following two forms:
            #   <count><type>  -or- <count><type>{delay}{units}
            # Use capturing parentheses for groups
            groups = re.split(r'([0-9]+)([a-z]+)([0-9]+)?([a-z]+)?', lcEntry)

            if const.DEBUG_SG_CMD_READOUT:
                logging.info("Seq entry={}, re.split groups, len={}: {}".format(
                    entry,len(groups),groups))

             # Look for a lone trigger entry or a ms/us delay
            if len(groups) == 1:
                if groups[0] == 't' or groups[0] == 'trig':
                    # seq=trig
                    self.sequence.append(\
                      SequenceEntry(SequenceAction.TRIG, 0, 1, True))
                    continue
                elif groups[0] == 'm' or groups[0] == 'msec' or\
                     groups[0] == 'u' or groups[0] == 'usec':
                    self.sequence.append(\
                      SequenceEntry(SequenceAction.DELAY,\
                                    DELAY_SCALER[groups[0]], 1, False))
                    continue
                else:
                    self.errMsg = "Invalid readout sequence entry \'{}\' in \'{}\'"\
                                  ", illformed!".format(entry,seqParamValue) 
                    return False

            # seq={count}{m|u|r|g|l|s}
            # Properly formed entry will have 6 groups.
            elif len(groups) != 6:
                self.errMsg = "Invalid readout sequence entry \'{}\' in \'{}\'"\
                              ", illformed!".format(entry,seqParamValue) 
                return False
            elif groups[0] != '':
                # Illformed repetition count otherwise group[0] would be empty
                self.errMsg = "Invalid readout sequence entry \'{}\' in \'{}\'"\
                              ", bad/missing count value!".format(entry,seqParamValue) 
                return False

            # group[1] == repetition count
            elif int(groups[1]) < 1:
                # Obvious bad value
                self.errMsg = "Invalid readout sequence entry \'{}\' in \'{}\'"\
                              ", count must be > 0!".format(entry,seqParamValue) 
                return False
            else:
                # Get integer value.
                frameRepeatCount = int(groups[1])

            # group[2] == frame type OR delay
            # NOTE: Sequence entry was converted to lower case before 
            #       the regex split was applied. All results are lower case.
            delay_us = 0
            isTriggered = False
            if groups[2] == 'm' or groups[2] == 'msec' or\
               groups[2] == 'u' or groups[2] == 'usec':
                # seq={count}msec -or- seq={count}usec
                # delay_us=value in usec
                delay_us = DELAY_SCALER[groups[2]] * frameRepeatCount;

                if delay_us > self.maxDelay_us:
                    self.errMsg = "Invalid readout sequence entry \'{}\' in \'{}\'"\
                                  ", delay must be <= {}sec!".format(
                                    entry,seqParamValue,
                                    self.maxDelay_us/const.MICROSECONDS_PER_SECOND) 
                    return False

                self.sequence.append(
                    SequenceEntry(SequenceAction.DELAY, delay_us, 1, isTriggered))
                continue
            
            # It's not a delay so it must be a frame read.
            # Check for short/long version of triggered frame read
            readType = groups[2]
            if readType[0] == 't':
                isTriggered = True
                if readType.find('trig',0,4) != -1:
                    readType = readType.replace('trig','',1)
                else:
                    readType = readType.replace('t','',1)

            # Check for short/long version of actual frame read type
            if readType == 'l' or readType == 'line':
                sequenceAction = SequenceAction.LINE
            elif readType == 'r' or readType == 'read':
                sequenceAction = SequenceAction.READ
            elif readType == 'g' or readType == 'global':
                sequenceAction = SequenceAction.GLOBAL
            elif readType == 's' or readType == 'subframe':
                sequenceAction = SequenceAction.SUBFRAME
            else:
                self.errMsg = "Invalid readout sequence entry \'{}\' in \'{}\'"\
                              ", bad/missing operation specifier!".format(entry,seqParamValue) 
                return False

            # seq=<count>{t|trig|<l|g|r|line|global|read>
            # If we make it here this is a frame readout
            # Increment total sequence count by repetition count 
            seqFrameCount += frameRepeatCount

            # Check to see if a delay was specified
            # seq=<count><l|g|r>{count}{m|u}
            # group[3] == delay (if present)
            delay_us = 0
            delayRepeatCount = 0
            if not groups[3] == None:
                # seq=<count><l|g|r><count>{m|u}
                # count is defined and contains only numerals since it's available here
                # in group[3]. 
                # Convert to actual integer
                delayRepeatCount = int(groups[3])

                # group[4] == delay units (required if delay present)
                if groups[4] == 'm' or groups[4] == 'msec' or\
                   groups[4] == 'u' or groups[4] == 'usec':

                    delay_us = delayRepeatCount * DELAY_SCALER[groups[4]]
                    if delay_us == 0 or delay_us > self.maxDelay_us:
                        self.errMsg = "Invalid readout sequence entry \'{}\' in \'{}\'"\
                                      ", delay must be <= {}sec!".format(
                                        entry,
                                        seqParamValue,self.maxDelay_us/const.MICROSECONDS_PER_SECOND) 
                        return False
                else:
                    self.errMsg = "Invalid readout sequence entry \'{}\' in \'{}\'"\
                                  ", invalid delay units!".format(entry,seqParamValue) 
                    return False

            self.sequence.append(SequenceEntry(
              sequenceAction, delay_us,frameRepeatCount, isTriggered))

        # If we get here we have a valid sequence definition.
        # Obtain counts from the sequence list
        self.totalDelay = 0           # Total delay time in microseconds
        self.numGlobalResets = 0      # Total number of global resets in one pass
        self.numLineResets = 0        # Total number of line resets in one pass
        self.numReads = 0             # Total number of reads in one pass
        self._num_subframes = 0       # Total number of subframes in one pass
        self.seqFrameCount = 0        # Total number of frames in one pass
        for s in self.sequence:
            if s.action == SequenceAction.TRIG:
                continue
            elif s.action == SequenceAction.DELAY:
                self.totalDelay += s.delay
            elif s.action == SequenceAction.LINE:
                self.numLineResets += s.count
                self.totalDelay += (s.count * s.delay)
            elif s.action == SequenceAction.GLOBAL:
                self.numGlobalResets += s.count
                self.totalDelay += (s.count * s.delay)
            elif s.action == SequenceAction.READ:
                self.numReads += s.count
                self.totalDelay += (s.count * s.delay)
            elif s.action == SequenceAction.SUBFRAME:
                self._num_subframes += s.count
                self.totalDelay += (s.count * s.delay)

        self.seqFrameCount = (self.numGlobalResets + self.numLineResets + self.numReads + self._num_subframes)

        # One final check: subframe and line/global/read are mutually exclusive with each other.
        # Verify that the defined sequence is either a subframe sequence or a ramp (line/globa/read).
        if (self.numLineResets != 0 or self.numGlobalResets != 0 or self.numReads != 0) and \
           self._num_subframes != 0:
            self.errMsg = 'Invalid readout sequence \'{}\' - \'subframe\' and  '\
                '\'global\'/\'line\'/\'read\' are mutually exclusive!'.format(entry,seqParamValue) 
            return False

        # Determine mode based frame counts...
        if self._num_subframes > 0:
            self._mode = const.ALLOWED_MODES[const.SUB_FRAME]
        else:
            self._mode = const.ALLOWED_MODES[const.UP_THE_RAMP]

        return True


    def processNframeParam(self, nframeParamValue, seqFrameCount):
        if seqFrameCount < const.MIN_NDRS[self._mode]:
            self.errMsg = "number of frames in sequence must be >= {}!"\
                          .format(const.MIN_NDRS[self._mode])
            return False

        if nframeParamValue != None: 
            # nframe is optional and defined...check it's value
            # Verify value we're going to is is > 0
            if nframeParamValue < const.MIN_NDRS['UpTheRamp']: 
                self.errMsg = "nframe value must be >= {}!"\
                              .format(const.MIN_NDRS['UpTheRamp'])
                return False

            self.numFrames = nframeParamValue         # Value ok, use it
        else:
            # nframe is optional and was not defined. Use sequence frame count
            self.numFrames = seqFrameCount

        # Verify that the nframe value, defined or default, is evenly 
        # divisible by the number of frames defined in the readout sequence
        if self.numFrames % seqFrameCount != 0:
            self.errMsg = "nframe must be evenly divisible by sequence "\
                          "frame count: nframe={}, frame count = {}!"\
                          .format(self.numFrames, seqFrameCount)
            return False

        return True


    def processSubFramePeriodParam(self, value):
        # If subframe_period is present then it's already been checked for a 
        # missing value by argparse. 
        # Regardless of the mode determined by the readout sequence check it for
        # a valid value.
        if not value == None:
            if value < default.MIN_SUBFRAME_PERIOD or \
               value > default.MAX_SUBFRAME_PERIOD or \
               value % 2 != 1:
                self.errMsg = '\'subframe_period\' value must be {} <= value <= {} and odd!'.format(
                    default.MIN_SUBFRAME_PERIOD, default.MAX_SUBFRAME_PERIOD)
                return False

            self._subframe_period = value
        else:
            self._subframe_period = 0

        return True


    def processArgs(self, argv):
        # Use the readoutCommand arg parser get the 'seq=' and optional 
        # 'nframe=' values storing them in the argparser.
        if not self.parseArgs(argv):
            return(False, self.errMsg, None, None, None, None, None)

        if not self.processSeqParam(self.args.seq):
            return(False, self.errMsg, None, None, None, None, None)

        if not self.processNframeParam(self.args.nframe, self.seqFrameCount):
            return(False, self.errMsg, None, None, None, None, None)

        if not self.processSubFramePeriodParam(self.args.subframe_period):
            return(False, self.errMsg, None, None, None, None, None)

        # Determine mode and perform mode based checks...
        if self._num_subframes > 0 and self._subframe_period > 0:
            mode = const.ALLOWED_MODES[const.SUB_FRAME]
        else:
            mode = const.ALLOWED_MODES[const.UP_THE_RAMP]

        if self._num_subframes > 0 and self._subframe_period == 0:
            self.errMsg = '\'subframe_period\' is not defined!'
            return(False, self.errMsg, None, None, None, None, None)


        return(True, "OK", mode, self.sequence, self.seqFrameCount, self.numFrames, self._subframe_period)    
