### Library imports ###

import logging               # For log generation
import numpy as np           # For generating image array for fits file
import select                # For dealing with non-blocking sockets with a timeout (stop cmd)
import shutil                # For copying fits files
import socket                # For dealing with all things sockets
import time                  # For generating timing and sleep calls

from astropy.io import fits  # For saving fits data
from pathlib import Path     # For dealing with sim data paths/filenames

### Stargrasp Sim Imports ###
from sgDefined import sgCommands as sgcmd
from sgDefined import sgResponse as sgresp
from sgDefined import sgConstants as const
from sgDefined import sgDefaults as default
from sgReadoutCmdUtil import SequenceAction

# Basic class that tries to mimic the relevant internal states of SGserver
class SGCamera:
    
    # Default constructor
    def __init__(self, cmdLineArgs, secondary, fitspipe):
        logging.info('Creating SGCamera...')
        
        self._initialized = False     # Camera as not been initialized via the 'init' command
        self._secondary = secondary   # Secondary client connection (interrupt,simtrig,rsimtrig)
        self.fitspipe = fitspipe

        # Readout and sequence info, Totals and current pass
        self.info = {
            'seqFrameCount': 0,   # frames per one pass of sequence
            'readout_count': 0,   # readouts in current pass of sequence
            'written_count': 0,   # writes in current pass of sequence
            'total_reads': 0,     # Total for readout command so far
            'total_writes': 0     # Total for readout command so far
        }

        self.timing = {
            'fread': cmdLineArgs.fread,   # Frame read (in ns)
            'frst':  cmdLineArgs.frst,    # FPGA reset (in ns) (unused)
            'grst':  cmdLineArgs.grst,    # Global reset (in ns)
            'lrst':  cmdLineArgs.lrst,    # Line reset (in ns)
            'seovh': cmdLineArgs.seovh,   # Subframe exposure time overhead (in ns)
            'sread': cmdLineArgs.sread,   # Subframe integration read (in ns)
            'srint': cmdLineArgs.srint,   # Subframe row integration time (in ns)
            'tend':  cmdLineArgs.tend,    # value in ns
            'tlat':  cmdLineArgs.tlat,    # value in ns

            'tFrameRead_s': 0.0,
            'tGlobalReset_s': 0.0,
            'tLineReset_s': 0.0,
            'tSubFrameRead_s': 0.0,
            'tSubFrameExpTime_s': 0.0,
            'tTotalSequenceTime_s': 0.0
        }

        self.settings = {
            'mode': default.MODE,
            'h': const.SENSOR_HEIGHT,   # rows to read
            'y': 0,                     # starting row
        
            # File naming
            'path': cmdLineArgs.tmpfspath,
            'fileNumber': 0,
            'seqNumber': 1,

            # readout command
            'sequenceList': [],
            'seqFrameCount': 0,         # reads/resets in one pass
            'nframe': 0,                # from readout command
            'subframe_period': 0        # from readout command
        }

        # FrameGen/Sim Data
        self.data = {
            'frameGenMode':    default.FRAMEGEN_MODE,
            'fixedPixelValue': default.FIXED_PIXEL_VALUE,
            'frameCount':      0,
            'path':            "",
            'baseFilename':    "",
            'filesPerSet':     0,
            'errorReason':     "" ,
            'srcFileIndex':    0
        }


        # Create an initial image array with the default 'frameGenMode'
        self.make_image()

        # fitspipe-get only configures itself after it has received the first 
        # frame. Since we don't want to loose any frames written by sgSim, we 
        # need to put a "dummy" frame on fitspiple. If this isn't done, then the
        # first real data file put on fits pipe will be lost
        # Write a dummy fits file to prime fitspipe. 
        self.fitspipe.putHdu(self.hdu, "Prime fitspipe", "startup framegen image")
        time.sleep(const.PRIME_FITSPIPE_WAIT)

        # The time at which the method sleepOrInterrupt either completes it
        # sleep or receives an 'interrupt'. This time value is used to set
        # the sequenceStartTime so that it more accurately reflects the point
        # in time that a "readout" (in our case sleep) has completed and just
        # before the data is written to fitspipe.  
        self.lastSleepCompletedAt = 0.0

        # When the last action in a sequence is a delay or while waiting for the
        # frame read time of the last frame of a sequence, it is necessary to
        # allow receipt of a 'trigger' command and not simply log and ignore it.
        # This is because there will be fluctuations in the timing of the 
        # delivery of the command since it's origin stems from a software 
        # interval timer interrupt in the CSS.
        self.acceptTrigDuringSleep = True      # allows receipt
        self._trig_received_during_sleep = 0

        # Flag used by sleepOrInterrupt() trigOrInterrupt() signaling that
        # an interrupt command has been received on the secondary channel.
        self.interruptCmdReceived = False


    # For printing the current status
    #   seqFrameCount: Total number of resets and/or reads in a single pass
    #                  of the defined readout sequence.
    #   readout_count: Total number of reads for current pass of the sequence.
    #   written_count: Total number of writes for current pass of the sequence.
    #   total_reads:   Total number of reads completed for the current
    #                  readout command.
    #   total_writes:  Total number of writes completed for the current
    #                  readout command.
    #   filenumber:    Base filenumber to be used in the final filename for 
    #                  files written.
    #   path:          Directory where files are set to be written to.
    #   readout_mode:  Configured readout mode ('UpTheRamp', 'SubFrame')
    #   yyy:           Number of rows to skip.
    #   rows:          Number of rows to read.
    #   trigger_count: Number of times trigger has been received.
    def __repr__(self):
        return str("""  seqFrameCount: %d     
      readout_count: %d
      written_count: %d
      total_reads  : %d
      total_writes : %d
      filenumber: %d
      path: %s
      readout_mode: %s
      yyy: %d
      rows: %d
      trigger_count: 0""" % (
            self.info['seqFrameCount'],
            self.info['readout_count'],
            self.info['written_count'],
            self.info['total_reads'],
            self.info['total_writes'],
            self.settings['fileNumber'],
            self.settings['path'],
            self.settings['mode'],
            self.settings['y'],
            self.settings['h']))


    def buildSimDataFilename(self, idx, ext):
        filename = self.data['baseFilename'] + \
                   ("." if self.data['baseFilename'].endswith('.') == False else "") + \
                   str(idx).zfill(3) + "." + ext
        return filename
        
        
    # Function for generating a test image fits HDU
    def make_image(self):
        generate = time.time()
        create = time.time()
        t1 = time.time()
        h = self.settings['h']
        w = const.SENSOR_WIDTH
        mode = self.data['frameGenMode']

        # Fixed Pixel Value
        if mode == "FixedPixel":
            self.hdu = fits.PrimaryHDU(
                np.full((w, h), self.data['fixedPixelValue'], dtype=const.BIT_TYPE))
        elif mode == "FrameCount":
            self.hdu = fits.PrimaryHDU(
                np.full((w, h), self.data['frameCount'], dtype=const.BIT_TYPE))
        elif mode == "ColumnCount":
            # Same as RowCount(i.e. original) but with Fortran ordering (column ordered)
            self.hdu = fits.PrimaryHDU(
                np.arange(h * w, dtype=const.BIT_TYPE).reshape(h, w, order='F'))
        else:
            # The original image generator with default C ordering (row ordered)
            self.hdu = fits.PrimaryHDU(
                np.arange(h * w, dtype=const.BIT_TYPE).reshape(h, w))

        # For testing, add one or more expected FITS cards 
#        self.hdu.header.append(card=('INSTRID','TheInstIdValue'))
#        logging.info('header {}'.format(self.hdu.header))

    # Function for simulating the timing given current camera settings
    # Returns:        succes - true/false as to successful or not
    #                 stsMsg - if success is false, contains reason why
    #         tGlobalReset_s - float full frame global reset time (seconds) 
    #           tLineReset_s - float full frame line reset time (seconds) 
    #           tFrameRead_s - float full frame read time (seconds) 
    #        tSubFrameRead_s - float full frame subframe read time (seconds)
    #     tSubFrameExpTime_s - float calculated subframe expsoure time (seconds)
    #   tTotalSequenceTime_s - float total time for one pass (seconds)
    def figure_timing(self, sequenceList, subFramePeriod):
        # Retrieve relevant settings used multiple times once here
        mode = self.settings['mode']

        # Total sequence time is mode based:
        # "UpTheRamp"
        #   totalSeqTime = trigger latency +
        #                  (numLineResets * lineResetTime) +
        #                  (numGlobalResets * globalResetTime) +
        #                  (numReads * frameReadtime)
        #
        # "SubFrame"
        #   totalSeqTime = trigger latency +
        #                  (numSubFrames * subFrameReadTime) +
        #                  (number of sequence interrupts * exposureTime)
        #   exposureTime = expTimeOverhead + 
        #                  (subFramePeriod * row integration time)
        #                  

        # All timing calculations can be completed regardless of mode (UpTheRamp or SubFrame).
        # All calcualted values may not actually be used.
        tFrameRead_s = self.timing['fread'] / const.NANOSECONDS_PER_SECOND

        tGlobalReset_s = (self.timing['frst'] + self.timing['grst']) / \
                         const.NANOSECONDS_PER_SECOND

        tLineReset_s = (self.timing['frst'] + self.timing['lrst']) / \
                       const.NANOSECONDS_PER_SECOND

        # NOTE: subFramePeriod may equal 0 if the readout sequence is not subframe
        tSubFrameRead_s = self.timing['sread'] / const.NANOSECONDS_PER_SECOND
        tSubFrameExpTime_s = (self.timing['seovh'] + (subFramePeriod * self.timing['srint'])) / \
                             const.NANOSECONDS_PER_SECOND

        tLat_s = self.timing['tlat'] / const.NANOSECONDS_PER_SECOND

        success = True
        stsMsg = "OK"
        tTotalSequenceTime_s = 0.0
        subFrameExpTimeCount = 0
        incurSubFrameExposureTime = False
        for entry in sequenceList:
            tActionTime_s = 0
            tDelayTime_us = 0.0
            if entry.action == SequenceAction.TRIG or \
               entry.action == SequenceAction.DELAY:
                # Just calculate possible delay from 'trig{N}{m|u|msec|usec}'
                # This entry has the .isTriggered = True but we'll defer to add
                # trigger related time values to the end since other .action 
                # entries can also have .isTriggered = True.
                tDelayTime_us = entry.count * entry.delay

                # Set flag to incur a subframe exposure time. It will actually
                # be counted (once) when a "subframe" sequence entry is processed.
                incurSubFrameExposureTime = True

            elif entry.action == SequenceAction.GLOBAL:     # mode='UpTheRamp'
                tActionTime_s = entry.count * tGlobalReset_s
                tDelayTime_us = entry.count * entry.delay

            elif entry.action == SequenceAction.LINE:       # mode='UpTheRamp'
                tActionTime_s = entry.count * tLineReset_s
                tDelayTime_us = entry.count * entry.delay

            elif entry.action == SequenceAction.READ:       # mode='UpTheRamp'
                tActionTime_s = (entry.count * tFrameRead_s) + \
                                 self.timing['tend']
                tDelayTime_us = entry.count * entry.delay

            elif entry.action == SequenceAction.SUBFRAME:   # mode='SubFrame'
                tActionTime_s = (entry.count * tSubFrameRead_s) + \
                                 self.timing['tend']
                tDelayTime_us = entry.count * entry.delay
                if mode == 'SubFrame':
                    for i in range(entry.count):
                        if entry.delay > 0 or entry.isTriggered:
                            incurSubFrameExposureTime = True

                        # NOTE: incurSubFrameExposureTime could also already be
                        # True if the preceeding sequence entry was a trigger or
                        # delay. Consecutive triggers/delays are not additive 
                        # for the incurred subframe expsoure time. We only want
                        # 1 for all interruptions preceeding the subframe read.
                        if incurSubFrameExposureTime:
                            subFrameExpTimeCount += 1
                            incurSubFrameExposureTime = False

            else:
                # We should never get here because an invalid entry will be 
                # caught when the sequence was processed by the SGInterface 
                # using the ReadoutHelper
                success = False
                stsMsg = "Unknown sequence entry {} in figure_timing()!".format(entry)
                break

            # If this entry is trigger add trigger latency...
            if entry.isTriggered:
                tActionTime_s += (entry.count * tLat_s)

            tTotalSequenceTime_s += (
                tActionTime_s + (tDelayTime_us/const.MICROSECONDS_PER_SECOND) )

        if mode == 'SubFrame':
            if subFrameExpTimeCount == 0: 
                # There is always at least one exposure time as part of the sequence
                subFrameExpTimeCount += 1

            # Subframe integration mode incurs an integration time at the
            # start of each triggered sequence.
            tTotalSequenceTime_s += (subFrameExpTimeCount * tSubFrameExpTime_s)

        if const.DEBUG_FIGURE_TIMING:
          logging.info( "DEBUG: figure_timing() {}:\n"
            "tDelayTime_us={}\ntGlobalReset_s={}s\ntLineReset_s={}s\n"
            "tFrameRead_s={}s\ntSubFrameRead_s={}\ntSubFrameExpTime_s={}\n"
            "subFrameExpTimeCount={}\ntTotalSequenceTime_s={}s".format(stsMsg, \
                tDelayTime_us, tGlobalReset_s, tLineReset_s, 
                tFrameRead_s, tSubFrameRead_s, tSubFrameExpTime_s, 
                subFrameExpTimeCount, tTotalSequenceTime_s))

        return (success, stsMsg, tGlobalReset_s, tLineReset_s, tFrameRead_s, tSubFrameRead_s, tSubFrameExpTime_s, tTotalSequenceTime_s)
        
    # Get next file number and circularly increment
    # Return value is in the range: 0 <= outNum <= MAX_FILES - 1
    def get_next_frame_no(self):
        outNum = self.settings['fileNumber']
        self.settings['fileNumber'] = (self.settings['fileNumber'] + 1) % const.MAX_FILES
        return outNum
    
    # Get next sequence number and circularly incerment.
    # Return value is in the range: 0 <= outNum <= MAX_SEQUENCES - 1
    def get_next_seq_no(self):
        outNum = self.settings['seqNumber']
        self.settings['seqNumber'] = (self.settings['seqNumber'] + 1) % const.MAX_SEQUENCES
        return outNum
    
    def executeReadout(self, returnSts, returnStsIdx):
        success = True
        logging.info("executeReadout entry")

        # Create dict for each read type and set with its read time
        frameTimes = {}
        frameTimes[SequenceAction.LINE]     = self.timing['tLineReset_s']
        frameTimes[SequenceAction.GLOBAL]   = self.timing['tGlobalReset_s']
        frameTimes[SequenceAction.READ]     = self.timing['tFrameRead_s']
        frameTimes[SequenceAction.SUBFRAME] = self.timing['tSubFrameRead_s']

        # Each sequence may have additional time following the final read. 
        # This will be represented here as tLastFrameExtra_s and only comes into 
        # play on the final frame.
        tLastFrameExtra_s = float(self.timing['tend'] / const.NANOSECONDS_PER_SECOND)

        # Number of frames for one pass of the sequence
        seqFrameCount = self.settings['seqFrameCount']

        # Total number of frames till readout command is complete
        nFrame = self.settings['nframe']

        # Reset some info parameters for this new readout command
        self.info['seqFrameCount'] = seqFrameCount
        self.info['total_reads'] = 0
        self.info['total_writes'] = 0
        self.interruptCmdReceived = False

        tReadoutStart = time.time()
        self.lastSleepCompletedAt = tReadoutStart

        for n in range(1, int(nFrame/seqFrameCount)+1):
            # The start of every sequence is the time of completion of the last 
            # readout. In sgSim's case, this is the time at completion of the 
            # most recent sleepOrInterrupt call.
            tSequenceStart = self.lastSleepCompletedAt

            seqNum = self.get_next_seq_no()
            logging.info("Sequence #%d Start - framesPerSequence=%d..." % (seqNum, seqFrameCount))
            
            # Reset some info parameters for this new sequence cycle
            self.info['readout_count'] = 0
            self.info['written_count'] = 0
            self.settings['fileNumber'] = 0

            # Build up extra logging info string
            frameGenMode = self.data['frameGenMode']
            if not frameGenMode == "Off":
                if frameGenMode == "FixedPixel":
                    frameGenInfoStr = "{}={}".format(frameGenMode, self.data['fixedPixelValue'])
                elif not frameGenMode == "FrameCount":
                    frameGenInfoStr = frameGenMode

            idealElapsedTime = 0.0
            # If this is the first sequence and it starts with 'subframe'
            # then there is an incurred exposure time. Otherwise, if it starts with
            # a trigger or delay, the sequence list processor below will pick up the
            # incurred exposure time.
            if n == 1 and self.settings['sequenceList'][0].action == SequenceAction.SUBFRAME:
                idealElapsedTime = self.timing['tSubFrameExpTime_s']

            tFitspipePut = 0.0   # Time just prior to fitspipe-put call
            for entry in self.settings['sequenceList']:
                if entry.action == SequenceAction.TRIG:
                    if self.settings['mode'] == 'SubFrame':
                        # A trigger interrupts the sequence so an exposure time is incurred
                        idealElapsedTime += self.timing['tSubFrameExpTime_s']

                    self.trigOrInterrupt()
                    tSequenceStart = time.time()
                    self.lastSleepCompletedAt = tSequenceStart

                elif entry.action == SequenceAction.DELAY:
                    if entry.isTriggered:
                        self.trigOrInterrupt()

                    idealElapsedTime += (entry.delay/const.MICROSECONDS_PER_SECOND)
                    if self.settings['mode'] == 'SubFrame':
                        # A delay interrupts the sequence so an exposure time is incurred
                        idealElapsedTime += self.timing['tSubFrameExpTime_s']

                    self.sleepOrInterrupt(tSequenceStart, idealElapsedTime)

                else:   # entry.action == SequenceAction.LINE/GLOBAL/READ/SUBFRAME
                    tFrameTime_s = frameTimes[entry.action]

                    for c in range(1, entry.count + 1):
                        if entry.isTriggered:
                            if self.settings['mode'] == 'SubFrame':
                                # A trigger interrupts the sequence so an exposure time is incurred
                                idealElapsedTime += self.timing['tSubFrameExpTime_s']

                            self.trigOrInterrupt()
                            tSequenceStart = time.time()
                            self.lastSleepCompletedAt = tSequenceStart

                        if entry.delay > 0:
                            idealElapsedTime += (entry.delay/const.MICROSECONDS_PER_SECOND)
    
                            # If this entry is triggered then we just incurred the exposure time.
                            if self.settings['mode'] == 'SubFrame' and not entry.isTriggered:
                                # A delay interrupts the sequence so an exposure time is incurred
                                idealElapsedTime += self.timing['tSubFrameExpTime_s']

                            self.sleepOrInterrupt(tSequenceStart, idealElapsedTime)

                        # NOTE: NEEDS TO OCCUR IN SEPARATE THREAD SO THAT WRITE
                        #       TIME DOES NOT GO AGAINST TOTAL SEQUENCE TIME
                        # Prep while waiting for each individual frame read time 
                        # (+ possible delay) to expire.
                        seqInfoStr = "seq={}, frame {} of {}".format(
                            seqNum, self.get_next_frame_no() + 1, self.settings['seqFrameCount'])

                        if frameGenMode == "Off":
                            # User supplied sim data. Build path/filename
                            idx = int(self.data['srcFileIndex'])
                            srcFilename = Path(self.data['path']) / self.buildSimDataFilename(idx, "fits")
                            idx = (idx + 1) % int(self.data['filesPerSet'])
                            self.data['srcFileIndex'] = int(idx)

                        elif frameGenMode == "FrameCount":
                            # Frame generator fits 
                            extraLoggingInfo = "{}={}".format(frameGenMode, self.data['frameCount'])
                        else:
                            extraLoggingInfo = frameGenInfoStr

                        # If this is the last frame allow for receipt of a trigger
                        # command during the sleep interval.
#                        if self.info['readout_count'] == seqFrameCount - 1:
#                        self.acceptTrigDuringSleep = True

                        # Wait for remaining frame time...
                        idealElapsedTime += tFrameTime_s
                        self.sleepOrInterrupt(tSequenceStart, idealElapsedTime)
                        # Adjust read counters...
                        self.info['readout_count'] += 1   # for this sequence
                        self.info['total_reads'] += 1     # for life of readout cmd

                        # Write(put) FITS file contents onto the fitspipe for consumption 
                        # by fitspipe-get
                        tFitspipePut = time.time()

                        if frameGenMode == "Off":
                            # User supplied sim data fits file
                            self.fitspipe.putFile(srcFilename, seqInfoStr)
                        else:
                            # Frame generator fits 
                            self.fitspipe.putHdu(self.hdu, seqInfoStr, extraLoggingInfo)

                        if const.DEBUG_EXECUTE_SEQUENCE:
                            logging.info("Time to queue fits: %.03fms"%\
                                ((time.time() - tFitspipePut) * const.MILLISECONDS_PER_SECOND))

                        # Adjust counters....
                        self.info['written_count'] += 1   # for this sequence
                        self.info['total_writes'] += 1    # for life of readout cmd
                        self.data['frameCount'] += 1      # for life of sim

                        # If FrameCount mode then make a new image for the next write...
                        if frameGenMode == "FrameCount":
                            self.make_image()

                        if self.interruptCmdReceived: break

                if self.interruptCmdReceived: break

            # Determine sequence status string
            if self.info['written_count'] == seqFrameCount:
                sequenceSts = "Complete"
            elif self.info['written_count'] != seqFrameCount:
                if self.interruptCmdReceived:
                    sequenceSts = "Interrupted"
                else:
                    sequenceSts = "Failure"

            # Time from start of sequence to just prior to fitspipe-put
            # Sequence could be interrupted before any data is written so
            # format info accordingly            
            if tFitspipePut == 0.0:
                lastWriteStr = "lastWrite=None"
            else:
                lastWriteStr = "lastWrite=%.9fs"%((tFitspipePut-tSequenceStart))

            logging.info("Sequence #%d %s, Total Sequence Time: "\
                         "Calculated=%.9fs, %s, Elapsed Time=%.9fs!" % 
                         (seqNum, sequenceSts, idealElapsedTime, 
                          lastWriteStr, (time.time()-tSequenceStart)))

            if sequenceSts == "Failure" or self.interruptCmdReceived: break

        tDone = time.time()

        # Set readout cmd status in the mutable return status list
        if sequenceSts == "Failure":
            returnSts[returnStsIdx] = "Failure"
        elif self.interruptCmdReceived:
            returnSts[returnStsIdx] = "Interrupted"
        else:
            returnSts[returnStsIdx] = "Complete"

        logging.info("Readout %s, Total Readout Command Time=%.9f!" % 
                     (returnSts[returnStsIdx], (tDone-tReadoutStart)))
    

    # init
    def init(self):
        time.sleep(default.INIT_TIME)
        self._initialized = True


    def standby(self):
        time.sleep(default.INIT_TIME)
        self._initialized = False


    # Method to look for either a sgcmd.SIMTRIG, sgcmd.RSIMTRIG, or
    # sgcmd.INTERRUPT command on the secondary connection. 
    #  sgcmd.SIMTRIG is only used internally for simulating a HW trigger.
    #  sgcmd.RSIMTRIG is only used internally for resetting the file index and
    #                 simulating a HW trigger.
    #  An sgcmd.INTERRUPT command on the secondary connection is per 
    #  documentation and causes current readout sequence to be interrupted thus
    #  ending the readout command.
    def trigOrInterrupt(self):
        t0 = time.time()
        logging.info("Waiting for \'%s\' or \'%s\'..."%(sgcmd.SIMTRIG, sgcmd.INTERRUPT))

        # Check whether a trigger was received during the last sleep period.
        # Timing is not exact for when the sgcmd.SIMTRIG or sgcmd.RSIMTRIG would
        # be received and when running at full speed, a either can occur right 
        # at the end of the previous sleep. 
        if self._trig_received_during_sleep > 0:
            self._trig_received_during_sleep -= 1
            return     # It's as though we just received the trigger

        self._secondary.connection.setblocking(0)
        while True:
            # timeout = 0 so we're going to poll so that we can 
            # periodically send an 'in progress' message back to the
            # client.
            ready = select.select([self._secondary.connection], [], [], 0)

            if ready[0]:
                cmd = self._secondary.recv().strip()

                logging.info("trigOrInterrupt: received cmd='%s'" % cmd)
                if cmd == sgcmd.SIMTRIG:
                    break
                elif cmd == sgcmd.RSIMTRIG:
                    self.data['srcFileIndex'] = int(0)
                    break
                elif cmd == sgcmd.INTERRUPT:
                    # Send inital 'OK' response.
                    self._secondary.sendResponse(sgresp.PREFIX.OK, "OK")
                    self.interruptCmdReceived = True
                    break
                else:
                    self._secondary.sendResponse(sgresp.PREFIX.ERROR, 
                        "Invalid cmd='%s' on the secondary command connection!"%(cmd))
            
        self._secondary.connection.setblocking(1)
        return

    # Method to either "sleep" for a period of time or look for
    # an sgcmd.INTERRUPT command on the secondary
    #  "sleeps" are waits simulating either a delay or readout time
    #  the duration of which is determined by the difference between an
    #  ideal and actual elapsed time. 
    #  An sgcmd.INTERRUPT command on the secondary connection is per 
    #  documentation and causes current readout sequence to be interrupted thus
    #  ending the readout command.
    def sleepOrInterrupt(self, startingFrom, idealElapsedTime):
        t0 = time.time()
        actualElapsedTime = t0 - startingFrom
        timeToWait = max(0, idealElapsedTime - actualElapsedTime)

        if not self.interruptCmdReceived:
            logging.info("Waiting for %.06fs, or \'%s'..."%(timeToWait,sgcmd.INTERRUPT))
        else:
            logging.info("Waiting for %.06fs, \'%s\' has been received..."%(timeToWait,sgcmd.INTERRUPT))

        self._secondary.connection.setblocking(0)
        while time.time()-t0 < timeToWait:
            try:
                # timeout = 0 so we're going to poll so that we can 
                # periodically send an 'in progress' message back to the
                # client.
                ready = select.select([self._secondary.connection], [], [], 0)

                if ready[0]:
                    cmd = self._secondary.recv().strip()
                    logging.info("sleepOrInterrupt: cmd='%s'" % cmd)
                    if cmd == sgcmd.INTERRUPT:
                        # Send inital 'OK' response.
                        self._secondary.sendResponse(sgresp.PREFIX.OK, "OK")
                        self.interruptCmdReceived = True
                        break;

                    # Check whether a trigger was received.
                    # Timing is not exact for when the sgcmd.SIMTRIG or 
                    # sgcmd.RSIMTRIG would be received and when running at full
                    # speed, a either can occur right at the end of the previous 
                    # sleep. 
                    elif cmd == sgcmd.SIMTRIG and self.acceptTrigDuringSleep:
                        # Save fact simtrig was received and continue to sleep
                        self._trig_received_during_sleep += 1
                        return

                    elif cmd == sgcmd.RSIMTRIG and self.acceptTrigDuringSleep:
                        # Save fact simtrig was received and continue to sleep
                        self.data['srcFileIndex'] = int(0)
                        self._trig_received_during_sleep += 1
                        return

                    else:
                        self._secondary.sendResponse(sgresp.PREFIX.ERROR, 
                            "Invalid cmd='%s' on this secondary command connection!"%(cmd))

            except socket.timeout as err:
                pass
        
        # All done. Save for next call to this method.
        self.lastSleepCompletedAt = time.time()

        self._secondary.connection.setblocking(1)
        return

    # Checks for valid frameGen mode.
    def dataFrameGenMode(self, inMode):
        # Check that supplied mode is "on the list"
        if const.ALLOWED_FRAMEGEN_MODES.count(inMode) == 0:
            logging.info("Bad frameGen mode selection")
            return False

        # Save new mode and create a new image if necessary.
        self.data['frameGenMode'] = inMode
        if inMode != "Off":
            self.make_image()
        return True
    
    # Checks for valid fixedPixelValue.
    def dataFixedPixelValue(self, inNumber):
        # Value must be >= 0
        if inNumber < 0:
            self.data['errorReason'] = "value must be >= 0"
            logging.info(self.data['errorReason'])
            return False
        
        # Save new FixedPixelValue and, if the current mode is FixedPixel then 
        # generate a new image with the new FixedPixelValue
        self.data['fixedPixelValue'] = inNumber
        if self.data['frameGenMode'] == "FixedPixel":
            self.make_image()
        return True
    
    # Checks for valid path.
    def dataPath(self, inPath):
        # Check to make sure this is a legitamate path, signal failure if not
        p = Path(inPath)
        if not p.exists():
            self.data['errorReason'] = "'{}' does not exist".format(inPath)
            logging.info(self.data['errorReason'])
            return False

        # Save new path.
        self.data['path'] = p
        self.data['srcFileIndex'] = int(0)    # reset current source file index
        return True

    # Checks for valid base filename.
    def dataBaseFilename(self, inName):
        # Save new base filename
        self.data['baseFilename'] = inName
        self.data['srcFileIndex'] = int(0)    # reset current source file index
        return True

    # Checks for valid number of files in a set of sim data files.
    def dataFilesPerSet(self, inNumber):
        # Value must be > 0
        if int(inNumber) <= 0:
            self.data['errorReason'] = "value must be > 0"
            logging.info(self.data['errorReason'])
            return False
        
        # Save new number of files per set
        self.data['filesPerSet'] = int(inNumber)
        self.data['srcFileIndex'] = int(0)    # reset current source file index
        return True

